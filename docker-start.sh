#! /bin/bash

virtualenv /greco-socios-env

source /greco-socios-env/bin/activate

/greco-socios-env/bin/python -m pip install --upgrade pip

pip install -r /greco-socios/requirements.txt

cd /greco-socios/

python /greco-socios/manage.py runsslserver --certificate /greco-socios/ssl/greco.bib.crt --key /greco-socios/ssl/greco.bib.key 0.0.0.0:80
