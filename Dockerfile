FROM python:2

# Install Apache
RUN apt-get -y update --fix-missing && \
	apt-get -y install \
	locales \
	wkhtmltopdf

RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz && \
	tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz && \
	mv wkhtmltox/bin/wkhtmlto* /usr/local/bin

RUN sed -i -e 's/# es_ES.UTF-8 UTF-8/es_ES.UTF-8 UTF-8/' /etc/locale.gen && \
	dpkg-reconfigure --frontend=noninteractive locales

ENV LANG es_ES.UTF-8
ENV LC_ALL es_ES.UTF-8

COPY . /greco-socios

RUN pip install virtualenv

CMD [ "bash", "/greco-socios/docker-start.sh" ]
