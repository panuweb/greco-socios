module.exports = {
	'root': true,
	'extends': [ 'plugin:@wordpress/eslint-plugin/recommended-with-formatting' ],
	'env': {
		'jquery': true,
		'browser': true,
		'es6': true,
	},
	'rules': {
		'quote-props': [ 'error', 'always' ],
		'padded-blocks': [ 'off' ],
		'padding-line-between-statements': [
			'error',
			{ 'blankLine': 'always', 'prev': '*', 'next': 'block' },
			{ 'blankLine': 'always', 'prev': 'block', 'next': '*' },
			{ 'blankLine': 'always', 'prev': '*', 'next': 'block-like' },
			{ 'blankLine': 'always', 'prev': 'block-like', 'next': '*' },
		],
		'jsdoc/require-jsdoc': 1,
		'import/no-unresolved': [
			'error',
			{
				'ignore': [
					'^\.\/(\.\.\/)?vendors/',
				],
			},
		],
	},
};
