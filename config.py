#encoding:utf-8
from app.settings import *

# Modo Depuración True/False
DEBUG = True


# Configuraciones de Base de Datos
DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'greco',                      # Nombre de la Base de Datos
		'USER': 'root',                       # Usuario de MySQL
		'PASSWORD': '',           # Contraseña del Usuario de MySQL
		'HOST': 'greco-socios-mariadb',            # Servidor de la base de datos
		'PORT': '3306',                       # Puerto del servidor de la base de datos
	}
}

# Zona Horaria: todas las opciones estan disponibles en:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'America/Argentina/Buenos_Aires'

# Idioma: todas las opciones estan disponibles en:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'es-AR'



# Una clave secreta para proporcionar firma criptográfica y debe ser un valor 
# único e impredecible. NO comparta esta clave con nadie.
SECRET_KEY = 'q3v>_~)[^e`8,Mb@=9]X=H!=fHW2~QhBqK<fRQ,jKe9yK[,$o9hemw~A?EjJ$no'

# Lista de dominios que Django tiene que servir. 
ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'greco.bib']
