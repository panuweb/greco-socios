#encoding:utf-8
from django.apps import apps as django_apps

from . import menu
from . import settings

def greco_common(request):
	
	# Usuario
	usuario = ''

	# Version
	version = {}

	# Apps
	apps = []
	for app  in django_apps.get_app_configs():
		apps += [app.name]

	return {
		'usuario': usuario,
		'menu': menu.MENU,
		'version': version,
		'apps': apps,
	}