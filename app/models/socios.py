#encoding:utf-8
from django.db import models
from django.core.validators import MaxValueValidator

class Socio(models.Model):
	"""Tabla socios"""

	class Meta:
		ordering = ['apellido', 'nombre']

	socio_id = models.AutoField(
		db_index=True,
		primary_key=True,
		unique=True,
		)

	socio_id_original = models.IntegerField(
		db_index=True,
	)

	status = models.CharField(
		choices=(
			('active', 'active'),
			('history', 'history'),
			('draft', 'draft'),
			),
		max_length=10,
		)

	num_socio = models.IntegerField(
		db_index=True,
		verbose_name="Numero de Socio",
		)

	apellido = models.CharField(
		db_index=True,
		max_length=30,
		)
	nombre = models.CharField(
		db_index=True,
		max_length=60,
		)
	documento = models.PositiveIntegerField(
		validators=[MaxValueValidator(99999999)],
		verbose_name="Numero de Documento",
		)
	fecha_nacim = models.DateField(
		verbose_name="Fecha de Nacimiento",
		)

	domicilio_calle = models.CharField(
		max_length=300,
		)

	domicilio_altura = models.IntegerField()

	domicilio_piso_dpto = models.CharField(
		max_length=300,
		)

	domicilio_aclaraciones = models.CharField(
		max_length=300,
		)

	domicilio_cod_postal = models.CharField(
		max_length=8,
		)

	domicilio_localidad_id = models.IntegerField(
		db_index=True,
		)

	contacto_email = models.EmailField(
		max_length=100,
		)

	contacto_telefono = models.CharField(
		max_length=30,
		)

	contacto_otro = models.TextField()

	autorizados = models.TextField()

	convenios = models.TextField()

	cobranza_tipo_choices = {
		'b': 'Biblioteca',
		'd': 'Domicilio',
		'e': 'Electrónica',
		}

	cobranza_tipo = models.CharField(
		choices=cobranza_tipo_choices.items(),
		max_length=1,
		)

	cobranza_monto_especial = models.DecimalField(
		max_digits=6,
		decimal_places=2
		)

	cobranza_biblioteca_motivo = models.CharField(
		max_length=300
		)

	cobranza_cobrador_id = models.IntegerField(
		db_index=True,
		blank = True,
		null=True,
		verbose_name="Cobrador",
		)

	cobranza_cobrador_domicilio_especial = models.CharField(
		max_length=1,
		)

	cobranza_cobrador_domicilio = models.TextField()

	cobranza_cobrador_dias_horas = models.TextField()

	cobranza_cobrador_otros = models.TextField()

	cobranza_epago_sistema_choices = {
		'0': ' -- Seleccionar -- ',
		'a': 'Débito Automático (tarjeta)',
		'd': 'Débito Directo (cuenta)',
		's': 'Suscripción Mercado Pago',
		#'b': 'Débito Automático NACIONAL',
		'c': 'Depósito en cuenta Nación',
		}

	cobranza_epago_sistema = models.CharField(
		choices=cobranza_epago_sistema_choices.items(),
		max_length=1,
		)

	cobranza_epago_entidad_nacional = models.CharField(
		max_length=50,
		)

	cobranza_epago_entidad_local_id = models.IntegerField(
		db_index=True,
		)

	cobranza_epago_tarjeta_marca = models.CharField(
		max_length=50,
		)

	cobranza_epago_tarjeta_numero = models.CharField(
		max_length=50,
		)

	cobranza_epago_tarjeta_vencimiento = models.CharField(
		max_length=4
		)

	cobranza_epago_cuenta_numero = models.CharField(
		max_length=50
		)

	cobranza_epago_cuenta_cbu = models.CharField(
		max_length=50
		)

	cobranza_titular_nombre = models.CharField(
		max_length=100
		)

	cobranza_titular_documento = models.CharField(
		max_length=20
		)

	cobranza_recibo_afip = models.BooleanField(
		default=False,
		verbose_name="Recibo AFIP.",
		)

	categoria_id = models.IntegerField(
		db_index=True,
		)

	categoria_protector_cuotas_adicionales = models.CharField(
		max_length=3
		)

	alta_fecha = models.DateField(
		db_index=True,
		verbose_name="Fecha de Alta",
		)

	alta_data = models.TextField(
		blank=True,
		verbose_name="Informacion de Alta",
		)

	baja_fecha = models.DateField(
		blank=True,
		db_index=True,
		default=None,
		null=True,
		verbose_name="Fecha de Baja",
		)

	baja_data = models.TextField(
		blank=True,
		verbose_name="Informacion de Baja",
		)

	observaciones = models.TextField(
		blank=True,
		verbose_name="Observaciones",
		)

	creacion_fecha = models.DateTimeField()

	def __unicode__(self):
		return '%s, %s' % (self.apellido, self.nombre)
