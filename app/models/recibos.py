#encoding:utf-8
from django.db import models
from django.core.validators import MaxValueValidator

class Recibo( models.Model ):
	class Meta:

		ordering = ['-recibo_id']

	recibo_id = models.AutoField(
		db_index=True,
		primary_key=True,
		unique=True,
		)
	cobranza = models.ForeignKey(
		'app.cobranza',
		db_index=True,
		)
	socio = models.ForeignKey(
		'app.socio',
		verbose_name="Numero del Socio",
		)
	categoria_id = models.IntegerField(
		verbose_name="Categoria del Socio",
		)
	cuota = models.CharField(
		db_index=True,
		max_length=10,
		verbose_name="Cuota Abonada",
		)
	cobranza_tipo_choices = {
		'b': 'Biblioteca',
		'd': 'Domicilio',
		'e': 'Electrónica',
		}

	cobranza_tipo = models.CharField(
		choices=cobranza_tipo_choices.items(),
		max_length=1,
		)
	cobrador_id = models.IntegerField(
		verbose_name="Cobrador Asignado",
		)
	epago_sistema_choices = {
		'0': ' -- Seleccionar -- ',
		'a': 'Débito Automático LOCAL',
		'b': 'Débito Automático NACIONAL',
		'c': 'Débito Directo NACIONAL',
		'd': 'Débito Directo LOCAL',
		}

	epago_sistema = models.CharField(
		choices=epago_sistema_choices.items(),
		max_length=1,
		)

	epago_entidad_nacional = models.CharField(
		max_length=50,
		)

	epago_entidad_local_id = models.IntegerField(
		blank=True,
		null=True,
		db_index=True,
		)

	recibo_afip = models.BooleanField(
		default=False,
		verbose_name="Recibo AFIP.",
		)

	pago_pagado = models.BooleanField(
		default=False,
		verbose_name="El recibo esta pagado.",
		)
	pago_fecha = models.DateField(
		blank=True,
		null=True,
		verbose_name="Fecha de pago",
		default=None
		)
	pago_cod = models.CharField(
		blank=True,
		max_length=30,
		null=True,
		verbose_name="Codigo de barras ingresado",
		default='',
		)

	anular_anulado = models.BooleanField(
		default=False,
		verbose_name="El recibo esta anulado.",
		)
	anular_motivo = models.CharField(
		blank=True,
		max_length=100,
		null=True,
		default="",
		verbose_name="Motivo por el que se anula el recibo.",
		)

	def cobrador_group(self):
		if 'b' == self.cobranza_tipo:
			group = 'b'

		elif 'e' == self.cobranza_tipo:
			group = 'b'

		elif 'd' == self.cobranza_tipo:
			group = self.cobrador_id

		else:
			group = '0'

		return group

	def __unicode__(self):
		return '%s - %s - %s' % ( self.recibo_id, self.cobranza, self.socio_id )
