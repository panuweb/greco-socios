#encoding:utf-8
from django.db import models
from django.core.validators import MaxValueValidator

class Cobranza(models.Model):
	class Meta:
		ordering = ['-periodo_anio','-periodo_mes']
	
	cobranza_id = models.AutoField(
		db_index=True,
		primary_key=True,
		unique=True,
		)
	nombre = models.CharField(
		blank=True,
		db_index=True,
		default=None,
		max_length=60,
		null=True,
		verbose_name="Nombre",
		)
	periodo_anio = models.IntegerField(
		db_index=True,
		verbose_name="Año",
		)
	periodo_mes = models.IntegerField(
		db_index=True,
		verbose_name="Mes",
		)
	cuota = models.CharField(
		db_index=True,
		max_length=10,
		verbose_name="Cuota Abonada",
		)
	notas = models.TextField(
		blank=True,
		default=None,
		null=True,
		verbose_name="Notas",
		)
	estado = models.CharField(
		default='new',#new / init / end / cancel
		max_length=14,
		verbose_name="Estado",
		)

	def __unicode__(self):
		return '%s-%02d %s' % (self.periodo_anio, self.periodo_mes, self.nombre)
