#encoding:utf-8
from django.shortcuts import render

from models.cobranzas import Cobranza

def socios(request):
	return render(request, 'socios.html', {})

def generar_cobranza(request):
	return render(request, 'cobranzas.html', {})
