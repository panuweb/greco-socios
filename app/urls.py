#encoding:utf-8
from django.conf.urls import  url
from django.views.generic import RedirectView

from .views import socios as socio_view, generar_cobranza

from pdf.recibos import recibos_cobranza
from pdf.detalle import detalle_cobranza
from pdf.afip import cobranza_recibos_afip
from pdf.listados import listado_cobradores, listado_asamblea

import api
from api import cobranzas
from api.socios import abm as socio_abm
from api.socios.general import socios_list, socios_details, socios_baja, socios_confirm_baja, socios_digibepe


urlpatterns = [
	url(r'^$', socio_view),

	url(r'^index.html$', RedirectView.as_view(url='/')),

	url(r'^api/socios/list.json', socios_list),
	url(r'^api/socios/detalles.json', socios_details),
	url(r'^api/socios/abm_form.json', socio_abm.form ),
	url(r'^api/socios/abm.json', socio_abm.save ),
	url(r'^api/socios/baja.json', socios_baja),
	url(r'^api/socios/abm_confirm_baja.json', socios_confirm_baja),
	url(r'^api/socios/digibepe.json', socios_digibepe),

	url(r'^socios/pdf/listado_cobradores.pdf', listado_cobradores),
	url(r'^socios/pdf/listado_asamblea.pdf', listado_asamblea),

	url(r'^api/cobranzas/list.json', cobranzas.list),
	url(r'^api/cobranzas/detalles.json', cobranzas.detalles),
	url(r'^api/cobranzas/create_form.json', cobranzas.create_form),
	url(r'^api/cobranzas/edit_form.json', cobranzas.edit_form),
	url(r'^api/cobranzas/generate.json', cobranzas.generate),
	url(r'^api/cobranzas/cancelar.json', cobranzas.cancelar),
	url(r'^api/cobranzas/update.json', cobranzas.update),

	url(r'^cobranzas/$', generar_cobranza),
	
	url(r'^cobranzas/pdf/cobranza-[0-9]{4}-[0-9]{2}-detalle.pdf', detalle_cobranza),
	url(r'^cobranzas/pdf/cobranza-[0-9]{4}-[0-9]{2}-recibos-cobradores.pdf', recibos_cobranza),
	url(r'^cobranzas/pdf/cobranza-[0-9]{4}-[0-9]{2}-recibos-afip.pdf', cobranza_recibos_afip),

	#url(r'^api/cobranzas/cobranzas.json', cobranzas.abm_cobranza),
]
