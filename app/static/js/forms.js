function forms() {
	var beforeSend, completeSend, errorSend, getFieldsData, sendData, validaFields;
	beforeSend = function(xhr, settings) {
		return true;
	};
	completeSend = function(xhr, status) {
		return true;
	};
	errorSend = function(xhr, status) {
		return true;
	};
	validaFields = function(fieldsData) {
		var i, message, valid, _i, _len;

		message = {};
		
		for (_i = 0, _len = fieldsData.length; _i < _len; _i++) {
			i = fieldsData[_i];
			valid = false;
			
			if (i["validate"]) {
				if (i["validations"]["Required"]) {
					if (i["value"].length === 0) {
						message = {
							'state': 'danger',
							'strong': 'Error de Validación:',
							'text': "El campo  <strong>" + i["name"] + "</strong>  no puede estar vacio."
						};
						break;
					}
				
				} 

				if (i["validations"]["Max"] !== undefined) {
					if (parseInt(i["value"]) > i["validations"]["Max"]) {
						message = {
							'state': 'warning',
							'strong': 'Error de Validación:',
							'text': "El campo  <strong>" + i["name"] + "</strong>  no puede ser mayor que " + i["validations"]["Max"] + "."
						};
						break;
					}
				}

				if (i["validations"]["Min"] !== undefined) {
					if (parseInt(i["value"]) < i["validations"]["Min"]) {
						message = {
							'state': 'warning',
							'strong': 'Error de Validación:',
							'text': "El campo  <strong>" + i["name"] + "</strong>  no puede ser menor que " + i["validations"]["Min"] + "."
						};
						break;
					}
				}

				if (i["validations"]["Number"] !== undefined) {
					if (isNaN(i["value"])) {
						message = {
							'state': 'warning',
							'strong': 'Error de Validación:',
							'text': "El campo  <strong>" + i["name"] + "</strong> debe ser un numero."
						};
						break;
					}
				}

				//console.error("No hay informacion para realizar la validación. Campo " + i['name'] + ".");
			}
			valid = true;
		}

		return {
			'valid': valid,
			'message': message
		};
	};
	getFieldsData = function($form) {
		var $fields, fieldsData;

		$fields = $form.find('input, textarea, select, button, keygen');
		fieldsData = [];
		
		// Si no encuentra campos devolvemos false
		if ($fields.length == 0) {
			return false
		}
		
		$fields.each(function() {
			var name, required, tag, type, val, validate;
			
			name = $(this).attr('name');
			tag = $(this).get(0).tagName;
			required = typeof($(this).data('required')) === 'undefined' ? false : true;
			validate = typeof($(this).data('validate')) === 'undefined' ? false : true;
			validations = {}
			
			if (tag === "TEXTAREA") {
				type = 'textarea';
			
			} else if (tag === "SELECT") {
				type = 'select';
			
			} else if (tag === "KEYGEN") {
				type = 'keygen';
			
			} else {
				type = $(this).attr('type');
				if (typeof type === 'undefined') {
					type = 'text';
				}
			}

			dataAtts = $(this).data()

			
			for (key in dataAtts) {
				val = dataAtts[key]
				if(key.indexOf('valida') == 0){
					tmp = key.substring(6)
					if(tmp != 'te') {
						validations[tmp] = val
					}
				}
			}
			
			if (typeof name !== 'undefined') {
				val = $(this).val();
				
				if (type === 'checkbox' || type === 'radio') {
					if (!$(this).is(':checked')) {
						return true;
					}
				}

				return fieldsData.push({
					'name': name,
					'value': val,
					'type': type,
					'validate': validate,
					'validations': validations
				});
			}
		});

		return fieldsData;
	};
	sendData = function(url, data, fnSuccess, fnError, csrftoken, fnBefore, fnComplete) {
		if (csrftoken == null) {
			csrftoken = '';
		}
		$.ajax({
			beforeSend: function(xhr, settings) {
				fnBefore(xhr, settings);
				return null;
			},
			complete: function(xhr, status) {
				fnComplete(xhr, status);
				return null;
			},
			data: csrftoken + '&json=' + JSON.stringify(data),
			success: function(data, status, xhr) {
				return fnSuccess(data, status, xhr);
			},
			type: 'POST',
			url: url
		});
		return null;
	};
	return {
		getFieldsData: getFieldsData,
		clearFieldsData: function($form) {
			var $fields;
			$fields = $form.find('input, textarea, select, button, keygen');
			$fields.each(function() {
				var tag;
				tag = $(this).get(0).tagName;
				if (tag === "SELECT") {
					if ($(this).data('default') !== void 0) {
						return $(this).val($(this).data('default'));
					} else {
						return $(this).find('option').first().prop('selected', true);
					}
				} else {
					if ($(this).data('default') !== void 0) {
						return $(this).val($(this).data('default'));
					} else {
						return $(this).val('');
					}
				}
			});
			return null;
		},
		sendForm: function($form, url, fnSuccess, fnError, csrftoken, validaForm, fnBefore, fnComplete) {
			var accion, data, fieldsData, i, valid, _i, _j, _len, _len1;
			if (fnError == null) {
				fnError = errorSend;
			}
			if (csrftoken == null) {
				csrftoken = false;
			}
			if (validaForm == null) {
				validaForm = true;
			}
			if (fnBefore == null) {
				fnBefore = beforeSend;
			}
			if (fnComplete == null) {
				fnComplete = completeSend;
			}
			fieldsData = getFieldsData($form);

			if (!fieldsData) {
				console.error('El formulario no tiene ningún campo.')
				return false
			}

			valid = validaFields(fieldsData);

			if ((validaForm && valid.valid) || !validaForm) {
				accion = '';
				for (_i = 0, _len = fieldsData.length; _i < _len; _i++) {
					i = fieldsData[_i];
					if (i["name"] === 'accion') {
						accion = i['value'];
						break;
					}
				}
				data = {
					'accion': accion,
					'data': {}
				};
				for (_j = 0, _len1 = fieldsData.length; _j < _len1; _j++) {
					i = fieldsData[_j];
					data.data[i["name"]] = i["value"];
				}
				if (csrftoken) {
					csrftoken = 'csrfmiddlewaretoken=' + $.cookie('csrftoken');
				} else {
					csrftoken = '';
				}
				sendData(url, data, fnSuccess, fnError, csrftoken, fnBefore, fnComplete);
			
			} else {
				fnError({
					'message': valid.message
				});
			}
			return null;
		}
	};
};

fr = forms();