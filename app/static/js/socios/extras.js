const showSocioDigibepe = function() {

	const title = 'Exportar DIGIBEPE';
	const modalButtons = [];

	helpers.modalShow({
		'title': title,
		'url': '/api/socios/digibepe.json',
		'template': 'socios/modal-digibepe',
		'size': 'lg',
		'buttons': modalButtons,

	}, function() {} );

	return;

};

const showIframe = function( url, title ) {
	helpers.modalIframeShow( {
		title,
		url,
	} );
};

const showListadoCobradores = function( e ) {
	showIframe(
		'/socios/pdf/listado_cobradores.pdf',
		'Listado cobradores',
	);
};

const showListadoAsamblea = function( e ) {
	const title = 'Listado Asamblea';

	const modalButtons = [
		{
			'id': 'listado-asamblea-genera-btn',
			'classes': [ 'btn-success' ],
			'name': 'Generar',
			'dataAttr': {
				'action': 'socios.listado-asamblea-generar',
			},
		},
	];

	helpers.modalShow( {
		'title': title,
		'template': 'socios/modal-listado-asamblea',
		'size': 'lg',
		'buttons': modalButtons,

	}, function() {} );

	return;
};

const showListadoAsambleaGenerar = function( e ) {
	const fecha_asamblea = $( '#fecha_asamblea' ).val();

	if ( undefined === fecha_asamblea || '' === fecha_asamblea ) {
		alert( "Debe ingresar una fecha" );

		return false;
	}

	showIframe(
		'/socios/pdf/listado_asamblea.pdf?fecha_asamblea=' + fecha_asamblea,
		'Listado socios habilitados para Asamblea',
	);
};
