/**
 * Ficha socio
 */
var showSocioFile = function( e ) {
	var id = 0;
	var modalButtons = [];

	id = parseInt( $( e.currentTarget ).attr( 'id' ) );

	modalButtons = [
		{
			'id': 'abm-guardar',
			'classes': [ 'btn-warning', 'mr-auto' ],
			'name': 'Editar',
			'dataAttr': {
				'action': 'socios.abm.editar',
				'socio-id': id,
			},
		},
	];


	helpers.modalShow({
		'title': 'Ficha de Socio',
		'url': '/api/socios/detalles.json',
		'template': 'socios/modal-detalles',
		'size': 'lg',
		'data': 'socio_id=' + id,
		'buttons': modalButtons,
	});

	return;
};


var editarSocio = function( btn ) {
	var socioId = parseInt( $( btn ).data( 'socio-id' ) );

	showSocioABMForm( socioId );
};
