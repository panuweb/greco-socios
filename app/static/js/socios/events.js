/**
 * Bind Events functions
 */
/* global helpers */
$( function() {

	// MainScreen events
	helpers.bindClick( 'socios.bajas.show', mostrarOcultarBajas );
	helpers.bindClick( 'socios.nuevo', showSocioABMForm );
	helpers.bindClick( 'socios.digibepe', showSocioDigibepe );
	helpers.bindClick( 'socios.listado-cobradores', showListadoCobradores );
	helpers.bindClick( 'socios.listado-asamblea', showListadoAsamblea );
	helpers.bindClick( 'socios.listado-asamblea-generar', showListadoAsambleaGenerar );

	// Socios Table
	helpers.bindClickSelector( '#socios_table tbody tr', showSocioFile );

	// Socios File
	helpers.bindClick( 'socios.abm.editar', editarSocio );

	// AMB Form
	helpers.bindOnChange( 'socios.abm.provincia', onChangeProvincia );
	helpers.bindOnChange( 'socios.abm.localidad', onChangeLocalidad );
	helpers.bindOnChange( 'socios.abm.cobranza.forma-pago', onChangeMedioPago );
	helpers.bindOnChange( 'socios.abm.cobranza.sistema', onChangeSistemaCobranza );
	helpers.bindOnChange( 'socios.abm.categoria', onChangeCategoria );
	helpers.bindClick( 'socios.abm.guardar', guardarSocio );
	helpers.bindClick( 'socios.abm.baja', bajaSocio );

	// Baja Form
	helpers.bindClick( 'socios.abm.confirmar_baja', confirmarBajaSocio );

	//$( 'body' ).on( 'click', '.abm-convenio-checkbox', convenioClick );
	$( 'body' ).on( 'click', '#abm-cobranza-domicilio-especial', domicilioCobranzaClick );

});
