/**
 * Lista de socios
 */
const initSociosTable = function() {
	const sociosTable = $( '#socios_table' ).dataTable( {
		'ajax': '/api/socios/list.json',
		'paging': false,
		'scrollX': true,
		'processing': true,
		'fixedHeader': {
			'headerOffset': 42,
		},
		'order': [
			[ 1, 'asc' ],
			[ 2, 'asc' ],
		],
		'columns': [
			{
				'data': 'num_socio',
				'width': '5%',
			}, {
				'data': 'apellido',
				'width': '20%',
			}, {
				'data': 'nombre',
				'width': '25%',
			}, {
				'data': 'categoria',
				'width': '20%',
				'searchable': false,
			}, {
				'data': 'tipo_cobranza',
				'width': '20%',
				'searchable': false,
			}, {
				'data': 'baja',
				'width': '100px',
				'visible': false,
				'searchable': false,
			}, {
				'data': 'actions',
				'className': 'actions',
				'width': '100px',
				'searchable': false,
			},
		],
		'language': {
			'zeroRecords': 'No se encontraron registros coincidentes.',
			'info': 'Mostrando _START_ a _END_ de _TOTAL_ registros.',
			'infoEmpty': 'Mostrando 0 a 0 de 0 registros.',
			'infoFiltered': '(filtrado de un total de _MAX_ registros)',
			'search': '',
			'paginate': {
				'next': 'Siguiente',
				'previous': 'Anterior',
			},
			'decimal': ',',
			'thousands': '.',
			'processing': '<div class="spinner-grow text-light" role="status"><span class="sr-only">Cargando...</span></div>',
		},
	} );

	$( '#socios_table_filter label input' )
		.addClass( 'form-control' )
		.attr( 'placeholder', 'Buscar' );
};


var mostrarOcultarBajas = function( target ) {
	sociosTable = $( '#socios_table' ).dataTable();
	$btn = $( target );

	if ( $btn.hasClass( 'active' ) ) {
		$btn.removeClass( 'active' ).text( 'Mostrar Bajas' ).blur();

		sociosTable.api().ajax.url( '/api/socios/list.json' ).load();
		sociosTable.api().column( 5 ).visible( false );

	} else {
		$btn.addClass( 'active' ).text( 'Ocultar Bajas' ).blur();

		sociosTable.api().ajax.url( '/api/socios/list.json?b=1' ).load();
		sociosTable.api().column( 5 ).visible( true );
	}
};
