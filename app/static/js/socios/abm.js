/**
 * Mostrar el formulario
 */

/* global helpers */

const showSocioABMForm = function( socioId, data, error ) {
	let title = 'Editar Socio';
	const modalButtons = [];

	// Si NO se estableció un numero de socio o este es cero, estamos cargando
	// uno nuevo.
	if ( isNaN( socioId ) || '0' === socioId ) {
		socioId = false;
		title = 'Nuevo Socio';
	}

	// Si se estableció un numero de socio, estamos EDITANDO uno
	if ( socioId ) {
		modalButtons.push(
			{
				'id': 'abm-baja',
				'classes': [ 'btn-danger', 'mr-auto' ],
				'name': 'Dar de baja',
				'dataAttr': {
					'action': 'socios.abm.baja',
				},
			}
		);
	}

	modalButtons.push(
		{
			'id': 'abm-guardar',
			'classes': [ 'btn-success' ],
			'name': 'Guardar',
			'dataAttr': {
				'action': 'socios.abm.guardar',
			},
		}
	);

	let queryString = 'socio_id=' + socioId;

	// Si se estableció el parámetro Data, es porque se produjo un error y se
	// tiene que volver a cargar el formulario con un mensaje de error y los
	// datos que ya cargó el usuario.
	if ( undefined !== data) {
		queryString += '&data=' + JSON.stringify( data );

		//console.log( queryString );
	}

	helpers.modalShow({
		'title': title,
		'url': '/api/socios/abm_form.json',
		'template': 'socios/modal-abm',
		'size': 'lg',
		'data': queryString,
		'buttons': modalButtons,
		'error': error,

	}, function() {
		$( '[data-socio-id]' ).data( 'socio-id', socioId );

		helpers.updateSelects( '#abm-provincia' );
		helpers.updateSelects( '#abm-localidad' );
		helpers.updateSelects( '#abm-cobranza-forma_pago' );
		helpers.updateSelects( '#abm-cobranza-cobrador' );
		helpers.updateSelects( '#abm-cobranza-sistema' );
		helpers.updateSelects( '#abm-cobranza-entidad-local' );
		helpers.updateSelects( '#abm-categoria' );

	});

	return;
};


/**
 * Domicilio
 */
var onChangeProvincia = function( e ) {
	var provinciaId = parseInt( $( e.currentTarget ).val() );
	var $localidad = $( '#abm-localidad' );

	// Mostramos solo las localidades de la provincia seleccionada.
	$localidad.find( 'option' ).each( function( idx, elm ) {
		$option = $( elm );

		if ( provinciaId === $option.data( 'provincia' ) || undefined === $option.data( 'provincia' ) ) {
			$option.removeClass( 'd-none' );

		} else {
			$option.addClass( 'd-none' );
		}
	});

	// Removemos la seleccion actual de la localidad.
	$localidad.val( 0 );

	// Mostramos/Ocultamos el campo localidad.
	if ( 0 !== provinciaId ) {
		$localidad.parents( '.form-group' ).removeClass( 'd-none' );

	} else {
		$localidad.parents( '.form-group' ).addClass( 'd-none' );
	}
};


var onChangeLocalidad = function( e ) {
	var localidadId = parseInt( $( e.currentTarget ).val() );

	// Mostramos/Ocultamos los campos de domicilio.
	if ( 0 !== localidadId ) {
		$( '#abm-calle' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-altura' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-dpto' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-aclaraciones' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-postal' ).parents( '.form-group' ).removeClass( 'd-none' );

	} else {
		$( '#abm-calle' ).parents( '.form-group' ).addClass( 'd-none' );
		$( '#abm-altura' ).parents( '.form-group' ).addClass( 'd-none' );
		$( '#abm-dpto' ).parents( '.form-group' ).addClass( 'd-none' );
		$( '#abm-aclaraciones' ).parents( '.form-group' ).addClass( 'd-none' );
		$( '#abm-postal' ).parents( '.form-group' ).addClass( 'd-none' );
	}
};


/**
 * Convenio
 */
/*
var convenioClick = function( e ) {
	var $convenioCheckbox = $( '#' + this.id );
	var convenioId = $( this ).data( 'convenio-id' );

	if ( $convenioCheckbox.is( ':checked' ) ) {
		$( '#abm-convenio-' + convenioId +'-data' ).removeClass( 'd-none' );

	} else {
		$( '#abm-convenio-' + convenioId +'-data' ).addClass( 'd-none' );
	}
};
*/


/**
 * Cobranza
 */
var hideCamposMedioPago = function() {
	$( '#abm-cobranza-motivo' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-domicilio-especial' ).parents( '.custom-control' ).addClass( 'd-none' );
	$( '#abm-cobranza-domicilio' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-dias-horas' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-otros-datos' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-cobrador' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-sistema' ).parents( '.form-group' ).addClass( 'd-none' );
};


var hideCamposSistemaCobranza = function() {
	$( '#abm-cobranza-entidad-local' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-tarjeta-numero' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-tarjeta-vencimiento' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-tarjeta' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-entidad' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-cbu' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-cuenta' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-titular-nombre' ).parents( '.form-group' ).addClass( 'd-none' );
	$( '#abm-cobranza-titular-documento' ).parents( '.form-group' ).addClass( 'd-none' );
};


var onChangeMedioPago = function( e ) {
	var medioPago = $( '#abm-cobranza-forma_pago' ).val();

	// Ocultamos todos los campos.
	hideCamposMedioPago();
	hideCamposSistemaCobranza();

	// Mostramos/Ocultamos los campos.
	if ( 'b' === medioPago ) {
		$( '#abm-cobranza-motivo' ).parents( '.form-group' ).removeClass( 'd-none' );

	} else if ( 'd' === medioPago ) {
		$( '#abm-cobranza-domicilio-especial' ).parents( '.custom-control' ).removeClass( 'd-none' );

		domicilioCobranzaClick();

		$( '#abm-cobranza-dias-horas' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-otros-datos' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-cobrador' ).parents( '.form-group' ).removeClass( 'd-none' );

	} else if ( 'e' === medioPago ) {
		$( '#abm-cobranza-sistema' ).parents( '.form-group' ).removeClass( 'd-none' );

		onChangeSistemaCobranza();
	}
};

var domicilioCobranzaClick = function( e ) {
	if ( $( '#abm-cobranza-domicilio-especial' ).is( ':checked' ) ) {
		$( '#abm-cobranza-domicilio' ).parents( '.form-group' ).removeClass( 'd-none' );

	} else {
		$( '#abm-cobranza-domicilio' ).parents( '.form-group' ).addClass( 'd-none' );
	}
};


var onChangeSistemaCobranza = function( e ) {
	var sistemaPago = $( '#abm-cobranza-sistema' ).val();

	// Ocultamos los campos.
	hideCamposSistemaCobranza();


	// Debito Automático Local
	if ( 'a' === sistemaPago ) {
		$( '#abm-cobranza-entidad-local' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-tarjeta-numero' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-tarjeta-vencimiento' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-nombre' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-documento' ).parents( '.form-group' ).removeClass( 'd-none' );

	// Debito Automático Nacional
	} else if ( 'b' === sistemaPago ) {
		$( '#abm-cobranza-tarjeta' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-entidad' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-tarjeta-numero' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-tarjeta-vencimiento' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-nombre' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-documento' ).parents( '.form-group' ).removeClass( 'd-none' );


	// Débito directo local.
	} else if ( 'd' === sistemaPago ) {
		$( '#abm-cobranza-entidad-local' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-cuenta' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-nombre' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-documento' ).parents( '.form-group' ).removeClass( 'd-none' );

	// Débito directo nacional.
	} else if ( 'c' === sistemaPago ) {
		$( '#abm-cobranza-entidad' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-cbu' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-nombre' ).parents( '.form-group' ).removeClass( 'd-none' );
		$( '#abm-cobranza-titular-documento' ).parents( '.form-group' ).removeClass( 'd-none' );
	}
};


var onChangeCategoria = function( e ) {
	var categoria = $( '#abm-categoria' ).val();

	// Socio Protector
	if ( '4' === categoria ) {
		$( '#abm-categoria-protector-adicionales' ).parents( '.form-group' ).removeClass( 'd-none' );

	// Todos los demás
	} else {
		$( '#abm-categoria-protector-adicionales' ).parents( '.form-group' ).addClass( 'd-none' );
	}
};


const guardarSocio = function( e ) {
	const socioId = $( '[data-socio-id]' ).data( 'socio-id' );

	const data = {
		'nombre': $( '#abm-nombre' ).val(),
		'apellido': $( '#abm-apellido' ).val(),
		'documento': $( '#abm-documento' ).val(),
		'fecha_nacim': $( '#abm-nacimiento' ).val(),

		'domicilio_provincia_id': $( '#abm-provincia' ).val(),
		'domicilio_localidad_id': $( '#abm-localidad' ).val(),
		'domicilio_calle': $( '#abm-calle' ).val(),
		'domicilio_altura': $( '#abm-altura' ).val(),
		'domicilio_piso_dpto': $( '#abm-dpto' ).val(),
		'domicilio_aclaraciones': $( '#abm-aclaraciones' ).val(),
		'domicilio_cod_postal': $( '#abm-postal' ).val(),

		'contacto_email': $( '#abm-email' ).val(),
		'contacto_telefono': $( '#abm-telefono' ).val(),
		'contacto_otro': $( '#abm-contacto-otros' ).val(),

		'autorizados': [],

		'convenios': [],

		'cobranza_tipo': $( '#abm-cobranza-forma_pago' ).val(),

		'cobranza_biblioteca_motivo': $( '#abm-cobranza-motivo' ).val(),

		'cobranza_cobrador_domicilio_especial': ( $( '#abm-cobranza-domicilio-especial' ).is( ':checked' ) ) ? 1 : 0,
		'cobranza_cobrador_domicilio': $( '#abm-cobranza-domicilio' ).val(),
		'cobranza_cobrador_dias_horas': $( '#abm-cobranza-dias-horas' ).val(),
		'cobranza_cobrador_otros': $( '#abm-cobranza-otros-datos' ).val(),
		'cobranza_cobrador_id': $( '#abm-cobranza-cobrador' ).val(),

		'cobranza_epago_sistema': $( '#abm-cobranza-sistema' ).val(),
		'cobranza_epago_entidad_local_id': $( '#abm-cobranza-entidad-local' ).val(),
		'cobranza_epago_tarjeta_numero': $( '#abm-cobranza-tarjeta-numero' ).val(),
		'cobranza_epago_tarjeta_vencimiento': $( '#abm-cobranza-tarjeta-vencimiento' ).val(),
		'cobranza_epago_tarjeta_marca': $( '#abm-cobranza-tarjeta' ).val(),
		'cobranza_epago_entidad_nacional': $( '#abm-cobranza-entidad' ).val(),
		'cobranza_epago_cuenta_cbu': $( '#abm-cobranza-cbu' ).val(),
		'cobranza_epago_cuenta_numero': $( '#abm-cobranza-cuenta' ).val(),
		'cobranza_monto_especial': '0.00',
		'cobranza_titular_nombre': $( '#abm-cobranza-titular-nombre' ).val(),
		'cobranza_titular_documento': $( '#abm-cobranza-titular-documento' ).val(),
		'cobranza_recibo_afip': ( $( '#abm-cobranza-recibo-afip' ).is( ':checked' ) ) ? 1 : 0,

		'observaciones': $( '#abm-cobranza-observaciones' ).val(),

		'categoria_id': $( '#abm-categoria' ).val(),
		'categoria_protector_cuotas_adicionales': $( '#abm-categoria-protector-adicionales' ).val(),

		'alta_fecha': $( '#abm-alta-fecha' ).val(),
		'alta_data': $( '#abm-alta-data' ).val(),
	};

	$( '.abm-convenio-checkbox' ).each( function( idx, elm ) {
		const convenioActive = $( elm ).is( ':checked' );
		const convenioId = $( elm ).attr( 'id' );

		if ( convenioActive ) {
			const convenioData = {
				'datos': [],
				'fecha_alta': $( '#' + convenioId + '-alta-fecha' ).val(),
				'fecha_baja': $( '#' + convenioId + '-baja-fecha' ).val(),
				'convenio_id': convenioId.replace( 'abm-convenio-', '' ),
				'convenio_nombre': $( '#' + convenioId + '-data h5' ).text(),
			};

			$( '.' + convenioId + '-datos' ).each( function() {
				convenioData.datos.push( {
					'nombre': $( elm ).find( 'label' ).text(),
					'value': $( elm ).find( 'input' ).val(),
				} );
			} );

			data.convenios.push( convenioData );
		}

	} );

	if ( socioId ) {
		data.socio_id = socioId;

	} else {
		data.socio_id = '0';
	}

	helpers.modalShow(
		{
			'title': 'Nuevo socio',
			'url': '/api/socios/abm.json',
			'template': 'socios/modal-save-result',
			'dismissButtonsText': 'Cerrar',
			'size': 'lg',
			'data': 'data=' + JSON.stringify( data ),
		},
		function() {
			const sociosTable = $( '#socios_table' ).dataTable();

			sociosTable.api().ajax.url( '/api/socios/list.json' ).load();
		},
		function( jqXHR ) {
			console.log( data );
			showSocioABMForm( data.socio_id, data, 'Se produjo un error al intentar guardar los datos. Por favor comuníquese con el administrador.' );
		}
	);
};


var bajaSocio = function( socioId ) {
	var title = 'Baja Socio';
	var modalButtons = [];

	var socioId = $( '[data-socio-id]' ).data( 'socio-id' );


	modalButtons.push(
		{
			'id': 'abm-confirmar-baja',
			'classes': [ 'btn-danger' ],
			'name': 'Dar de baja',
			'dataAttr': {
				'action': 'socios.abm.confirmar_baja',
			},
		}
	);

	helpers.modalShow({
		'title': title,
		'url': '/api/socios/baja.json',
		'template': 'socios/modal-baja',
		'size': 'lg',
		'data': 'socio_id=' + socioId,
		'buttons': modalButtons,

	}, function() {
		$( '[data-socio-id]' ).data( 'socio-id', socioId );
	});

	return;
};

const confirmarBajaSocio = function( socioId ) {
	socioId = $( '[data-socio-id]' ).data( 'socio-id' );

	const data = {
		'socio_id': socioId,
		'baja_fecha': $( '#abm-baja-fecha' ).val(),
		'baja_data': $( '#abm-baja-data' ).val(),
	};

	helpers.modalShow( {
		'title': 'Baja socio',
		'dismissButtonsText': 'Cerrar',
		'size': 'lg',
		'url': '/api/socios/abm_confirm_baja.json',
		'template': 'socios/modal-save-result',
		'data': 'data=' + JSON.stringify( data ),
	} );
};
