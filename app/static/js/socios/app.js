/* global helpers */

// Cargamos el template principal
helpers.loadTemplate( 'socios/main', undefined, function( html ) {
	$( '.container article' ).html( html );

	helpers.updateInterface(
		'Socios', // Título
		[ // Barra de botones
			{
				'classes': [ 'btn-primary' ],
				'name': 'Listado Asamblea',
				'dataAttr': {
					'action': 'socios.listado-asamblea',
				},
			},
			{
				'classes': [ 'btn-primary' ],
				'name': 'Listado Cobradores',
				'dataAttr': {
					'action': 'socios.listado-cobradores',
				},
			},
			{
				'classes': [ 'btn-primary mr-5' ],
				'name': 'DIGIBEPE',
				'dataAttr': {
					'action': 'socios.digibepe',
				},
			},
			{
				'classes': [ 'btn-success' ],
				'name': 'Nuevo Socio',
				'dataAttr': {
					'action': 'socios.nuevo',
				},
			},
			{
				'classes': [ 'btn-primary' ],
				'name': 'Mostrar Bajas',
				'dataAttr': {
					'action': 'socios.bajas.show',
				},
			},
		]
	);

	initSociosTable();
});
