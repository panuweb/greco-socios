/* global helpers, initCobranzaTable */

/**
 * Actualizamos la interfaz.
 */


// Cargamos el template principal
helpers.loadTemplate( 'cobranzas/main', undefined, function( html ) {
	$( '.container article' ).html( html );

	helpers.updateInterface(
		'Cobranzas', // Título
		[ // Barra de botones
			{
				'classes': [ 'btn-success' ],
				'name': 'Nueva Cobranza',
				'dataAttr': {
					'action': 'cobranzas.nueva',
				},
			},
		]
	);

	initCobranzaTable();
} );


/*

initActions = function() {

	ac.attachAction("cobranza.cancel", function($this) {
		id = $this.parents('tr').data('id');
		$('#cobranza_accion').val('cancel');
		$('#cobranza_id').val(id);
		
		$response = $('#form_cobranza').find('.form-response');

		fr.sendForm(
			$('#form_cobranza'), 
			'/api/cobranzas/cobranzas.json', 
			manageResponse,
			manageResponse, 
			true,
			false,
			function() {
				$("#form_cobranza").addClass('loading')
			},
			function() {
				$("#form_cobranza").removeClass('loading')
			}
		)
	});

	ac.attachAction( 'cobranza.edit', function($this) {
		$tr = $this.parents('tr');
		id = $tr.data('id');
		
		$('#cobranza_accion').val('update');
		$('#cobranza_id').val(id);

		$('#cobranza_nombre').val($tr.find('td.nombre').text())
		$('#cobranza_notas').val($tr.find('td.notas').text())
		$('#cobranza_mes').val($('#cobranza_mes').data('default')).prop('disabled', true)
		$('#cobranza_anio').val($('#cobranza_anio').data('default')).prop('disabled', true)
		$('#cobranza_cuota').val($('#cobranza_cuota').data('default')).prop('disabled', true)
	});

};

*/
