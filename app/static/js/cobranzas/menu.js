/**
 * 
 */

/* global helpers */

/**
 * Mostrar el formulario
 */
const showMenu = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );

	helpers.modalShow(
		{
			'title': 'Menu Cobranza',
			'url': '/api/cobranzas/detalles.json',
			'template': 'cobranzas/modal-menu',
			'data': 'cobranza_id=' + JSON.stringify( cobranzaId ),
			'dismissButtonsText': 'Cerrar',
			'size': '',
		},
		function() {}
	);
};

const showIframe = function( cobranzaId, url, title ) {
	const modalButtons = [
		{
			'classes': [ 'btn-ligth' ],
			'name': 'Cerrar',
			'dataAttr': {
				'action': 'cobranzas.menu',
				'cobranza-id': cobranzaId,
			},
		},
	];

	helpers.modalIframeShow( {
		title,
		url,
		'dismissButton': false,
		'buttons': modalButtons,
	} );
};

const showDetalles = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );
	const cobranzaPeriodo = $elm.data( 'cobranza-periodo' );

	showIframe(
		cobranzaId,
		'/cobranzas/pdf/cobranza-' + cobranzaPeriodo + '-detalle.pdf?cobranza=' + cobranzaId,
		'Detalle de Cobranza',
	);
};

const showRecibos = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );
	const cobranzaPeriodo = $elm.data( 'cobranza-periodo' );

	showIframe(
		cobranzaId,
		'/cobranzas/pdf/cobranza-' + cobranzaPeriodo + '-recibos-cobradores.pdf?cobranza=' + cobranzaId,
		'Recibos cobradores',
	);
};

const showRecibosAfip = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );
	const cobranzaPeriodo = $elm.data( 'cobranza-periodo' );

	showIframe(
		cobranzaId,
		'/cobranzas/pdf/cobranza-' + cobranzaPeriodo + '-recibos-afip.pdf?cobranza=' + cobranzaId,
		'Recibos Afip',
	);
};