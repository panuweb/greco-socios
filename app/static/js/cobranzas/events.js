/**
 * Bind Events functions
 */

/* global helpers */

$( function() {
	helpers.bindClick( 'cobranzas.nueva', showCobranzasCreateForm );
	helpers.bindClick( 'cobranzas.editar', showCobranzasEditForm );
	helpers.bindClick( 'cobranzas.menu', showMenu );
	helpers.bindClick( 'cobranzas.informe', showDetalles );
	helpers.bindClick( 'cobranzas.recibos', showRecibos );
	helpers.bindClick( 'cobranzas.afip', showRecibosAfip );

	helpers.bindClick( 'cobranzas.generate', generate );
	helpers.bindClick( 'cobranzas.cancelar', cancelar );
	helpers.bindClick( 'cobranzas.update', update );
} );
