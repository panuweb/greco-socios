/**
 * Mostrar el formulario
 */
/* global helpers */

/**
 *
 */
const showCobranzasCreateForm = function() {
	const modalButtons = [];

	modalButtons.push(
		{
			'id': 'create-generar',
			'classes': [ 'btn-success' ],
			'name': 'Generar',
			'dataAttr': {
				'action': 'cobranzas.generate',
			},
		}
	);

	helpers.modalShow(
		{
			'title': 'Nueva Cobranza',
			'url': '/api/cobranzas/create_form.json',
			'template': 'cobranzas/modal-create',
			'buttons': modalButtons,
		},
		function() {}
	);
};

const showCobranzasEditForm = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );

	const modalButtons = [];

	modalButtons.push(
		{
			'id': 'create-generar',
			'classes': [ 'btn-success' ],
			'name': 'Guardar',
			'dataAttr': {
				'action': 'cobranzas.update',
			},
		}
	);

	helpers.modalShow(
		{
			'title': 'Editar Cobranza',
			'url': '/api/cobranzas/edit_form.json',
			'template': 'cobranzas/modal-editar',
			'data': 'cobranza_id=' + cobranzaId,
			'buttons': modalButtons,
		},
		function() {}
	);
};