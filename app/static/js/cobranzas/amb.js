
const generate = function( e ) {
	const modalButtons = [];

	data = {
		'nombre': $( '#cobranza_nombre' ).val(),
		'mes': $( '#cobranza_mes' ).val(),
		'anio': $( '#cobranza_anio' ).val(),
		'cuota': $( '#cobranza_cuota' ).val(),
		'notas': $( '#cobranza_notas' ).val(),
	};

	modalButtons.push(
		{
			'id': 'create-generar',
			'classes': [ 'btn-light' ],
			'name': 'Cerrar',
			'dataAttr': {
				'action': 'cobranzas.generate',
			},
		}
	);

	helpers.modalShow(
		{
			'title': 'Generar cobranza',
			'url': '/api/cobranzas/generate.json',
			'template': 'cobranzas/modal-generate-result',
			'buttons': modalButtons,
			'dismissButton': false,
			'data': 'data=' + JSON.stringify( data ),
		},
		function() {
			const sociosTable = $( '#cobranza_table' ).dataTable();

			sociosTable.api().ajax.url( '/api/cobranzas/list.json' ).load();
		},
		function( jqXHR ) {
			$( '#modal' ).modal( 'hide' );
		}
	);
};

const cancelar = function( e ) {
	const $elm = $( e );
	const cobranzaId = $elm.data( 'cobranza-id' );
	const periodo = $elm.data( 'cobranza-periodo' );

	const consultaUsuario = confirm( 'Confirma que desea cancelar la cobranza: ' + cobranzaId + ' del mes ' + periodo );

	if ( consultaUsuario ) {
		helpers.modalShow(
			{
				'title': 'Cancelar cobranza',
				'url': '/api/cobranzas/cancelar.json',
				'template': 'cobranzas/modal-generate-result',
				'data': 'data=' + JSON.stringify( { 'cobranza_id': cobranzaId } ),
			},
			function() {
				const sociosTable = $( '#cobranza_table' ).dataTable();

				sociosTable.api().ajax.url( '/api/cobranzas/list.json' ).load();
			},
			function( jqXHR ) {
				$( '#modal' ).modal( 'hide' );
			}
		);
	}
	/*
	*/
};

const update = function( e ) {
	data = {
		'id': $( '#cobranza_id' ).val(),
		'nombre': $( '#cobranza_nombre' ).val(),
		'notas': $( '#cobranza_notas' ).val(),
	};

	helpers.modalShow(
		{
			'title': 'Actualizar cobranza',
			'url': '/api/cobranzas/update.json',
			'template': 'cobranzas/modal-generate-result',
			'data': 'data=' + JSON.stringify( data ),
		},
		function() {
			const sociosTable = $( '#cobranza_table' ).dataTable();

			sociosTable.api().ajax.url( '/api/cobranzas/list.json' ).load();
		},
		function( jqXHR ) {
			$( '#modal' ).modal( 'hide' );
		}
	);
};
