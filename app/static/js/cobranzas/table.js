/**
 * 
 */

/**
 * Lista de socios
 */
const initCobranzaTable = function() {

	$( '#cobranza_table' ).dataTable( {
		'ajax': '/api/cobranzas/list.json',
		'paging': false,
		'scrollX': true,
		'processing': true,
		'order': [
			[ 0, 'desc' ],
		],
		'columns': [
			{
				'data': 'actions',
				'className': 'actions',
				'width': '60px',
				'searchable': false,
			},
			{
				'data': 'cobranza',
				'width': '5%',
			},
			{
				'data': {
					'_': 'periodo.display',
					'sort': 'periodo.sort',
				},
				'width': '10%',
			},
			{
				'data': 'nombre',
				'width': '25%',
				'orderable': false,
			},
			{
				'data': 'cuota',
				'width': '15%',
				'orderable': false,
			},
			{
				'data': 'notas',
				'width': '45%',
				'orderable': false,
			},
		],
		'language': {
			'zeroRecords': 'No se encontraron registros coincidentes.',
			'info': 'Mostrando _START_ a _END_ de _TOTAL_ registros.',
			'infoEmpty': 'Mostrando 0 a 0 de 0 registros.',
			'infoFiltered': '(filtrado de un total de _MAX_ registros)',
			'search': '',
			'paginate': {
				'next': 'Siguiente',
				'previous': 'Anterior',
			},
			'decimal': ',',
			'thousands': '.',
			'processing': '<div class="spinner-grow text-light" role="status"><span class="sr-only">Cargando...</span></div>',
		},
	});

	$( '#cobranza_table_filter label input' )
		.addClass( 'form-control' )
		.attr( 'placeholder', 'Buscar' );
};
