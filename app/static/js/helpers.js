const templates = {};

const helpers = {
	bindClick( name, cb ) {
		$( 'body' ).on( 'click', '.btn[data-action="' + name + '"]', function( e ) {
			e.preventDefault();

			cb( e.currentTarget );

			return null;
		} );
	},

	bindClickSelector( selector, cb ) {
		$( 'body' ).on( 'click', selector, function( e ) {
			e.preventDefault();

			cb( e );

			return null;
		});
	},

	bindKeyUp( name, cb ) {
		$( 'body' ).on( 'keyup', 'input.form-control[data-action="' + name + '"]', function( e ) {
			e.preventDefault();

			cb( e.currentTarget );

			return null;
		});
	},

	bindOnChange( name, cb ) {
		$( 'body' ).on( 'change', '[data-action="' + name + '"]', function( e ) {
			e.preventDefault();

			cb( e );

			return null;
		});
	},

	modalShow( params, fnSuccess, fnError ) {
		const $modal = $( '#modal' );
		const $modalBody = $( '#modal .modal-body' );
		const $modalTitle = $( '#modal .modal-title' );
		const $modalFooter = $( '#modal .modal-footer' );

		if ( undefined === params.size ) {
			$( '#modal .modal-dialog' ).addClass( 'modal-xl' );

		} else if ( '' === params.size ) {
			$( '#modal .modal-dialog' ).removeClass( 'modal-xl' );
			$( '#modal .modal-dialog' ).removeClass( 'modal-lg' );
			$( '#modal .modal-dialog' ).removeClass( 'modal-sm' );

		} else if ( '' !== params.size ) {
			$( '#modal .modal-dialog' ).addClass( 'modal-' + params.size );
		}

		if ( undefined === params.dismissButtons ) {
			params.dismissButtons = true;
		}

		if ( undefined === params.dismissButtonsText ) {
			params.dismissButtonsText = 'Cancelar';
		}

		if ( undefined === fnSuccess ) {
			fnSuccess = function() {
				return true;
			};
		}

		if ( undefined === fnError ) {
			fnError = function() {
				return true;
			};
		}

		// Muestra la ventana modal.
		$modal.modal({
			backdrop: 'static',
		});

		$.ajax({
			beforeSend() {
				$modalBody.html( '<div class="modal-loader"><div class="spinner-grow" role="status"><span class="sr-only">Cargando...</span></div></div>' );
				$modalTitle.html( params.title );
				$modalFooter.html( helpers.modalGeneraFooterButtons( params.buttons, params.dismissButton, params.dismissButtonsText ) );
			},
			complete( objeto, exito ) {
				return true;
			},
			'data': params.data,
			success( dat ) {
				//console.log( dat );

				if ( undefined !== params.error ) {
					dat.error = params.error;
				}

				helpers.loadTemplate( params.template, dat, function( html ) {
					$modalBody.html( html );

					fnSuccess( dat );
				});
			},
			error( jqXHR, textStatus, errorThrown ) {
				//console.log( jqXHR );

				fnError( jqXHR );
			},
			'type': 'GET',
			'url': params.url,
		});
	},

	modalIframeShow( params, fnSuccess, fnError ) {
		const $modal = $( '#modal' );
		const $modalBody = $( '#modal .modal-body' );
		const $modalTitle = $( '#modal .modal-title' );
		const $modalFooter = $( '#modal .modal-footer' );

		if ( undefined === params.size ) {
			$( '#modal .modal-dialog' ).addClass( 'modal-xl' );

		} else if ( '' !== params.size ) {
			$( '#modal .modal-dialog' ).addClass( 'modal-' + params.size );
		}

		if ( undefined === params.dismissButtons ) {
			params.dismissButtons = true;
		}

		if ( undefined === params.dismissButtonsText ) {
			params.dismissButtonsText = 'Cancelar';
		}

		if ( undefined === fnSuccess ) {
			fnSuccess = function() {
				return true;
			};
		}

		if ( undefined === fnError ) {
			fnError = function() {
				return true;
			};
		}

		// Muestra la ventana modal.
		$modal.modal({
			backdrop: 'static',
		});

		const $iframe = $( '<iframe class="d-none">' );
		const $loader = $( '<div class="modal-loader"><div class="spinner-grow" role="status"><span class="sr-only">Cargando...</span></div></div>' );

		$iframe.attr( 'src', params.url );

		$iframe.on( 'load', function() {
			$loader.remove();
			$iframe.removeClass( 'd-none' );
		} );

		$modalBody.empty();
		$modalBody.append( $loader );
		$modalBody.append( $iframe );

		$modalTitle.html( params.title );
		$modalFooter.html( helpers.modalGeneraFooterButtons( params.buttons, params.dismissButton, params.dismissButtonsText ) );
	},

	modalGeneraFooterButtons( buttons, dismissButton, dismissButtonsText ) {
		let buttonsHTML = '';
		let i = 0;

		if ( undefined === dismissButton ) {
			dismissButton = true;
		}

		if ( undefined === dismissButtonsText ) {
			dismissButtonsText = 'Cancelar';
		}

		if ( undefined !== buttons ) {
			for ( i = 0; i < buttons.length; i++ ) {
				const btn = buttons[ i ];
				const attrData = [];

				for ( const j in btn.dataAttr ) {
					attrData.push( 'data-' + j + '="' + btn.dataAttr[ j ] + '"' );
				}

				buttonsHTML += '<a href="#" ';

				if ( undefined !== btn.id ) {
					buttonsHTML += 'id="' + btn.id + '" ';
				}

				buttonsHTML += 'class="btn ' + btn.classes.join( ' ' ) + '" ';
				buttonsHTML += attrData.join( ' ' );
				buttonsHTML += '>' + btn.name + '</a>';

			}
		}

		// Dismiss modal button
		if ( undefined !== dismissButton && false !== dismissButton ) {
			buttonsHTML += '<a class="btn btn-light" href="#" data-dismiss="modal">' + dismissButtonsText + '</a>';
		}

		return buttonsHTML;
	},

	loadTemplate( tmpl, data, cb ) {
		let template = null;

		if ( undefined === data ) {
			data = {};
		}

		if ( undefined === cb ) {
			cb = function() {
				return true;
			};
		}

		if ( undefined === templates[ tmpl ]) {
			jQuery.get( '/static/js_templates/' + tmpl + '.html', function( resp ) {
				templates[ tmpl ] = Handlebars.compile( resp );
				helpers.loadTemplate( tmpl, data, cb );
			});

			return;
		}

		template = templates[ tmpl ];

		cb( template( data ) );

		return;
	},

	updateSelects( selector ) {
		const value = $( selector ).data( 'value' );

		$( selector + ' option' ).each( function( idx, elm ) {
			if ( String( value ) === $( elm ).val() ) {
				$( elm ).prop( 'selected', true );
			}
		});

		$( selector ).change();
	},

	updateInterface( title, toolbar ) {
		$( '#page-title' ).html( title );
		$( '.header-actions' ).html( helpers.modalGeneraFooterButtons( toolbar, false ) );
	},
};
