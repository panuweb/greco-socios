function utils() {

	var succesFunction;
	
	succesFunction = function() {
		return true;
	};
	
	return {
		existsElm: function(elm) {
			if ($(elm).length) {
				return true;
			} else {
				return false;
			}
		},
		serializeURL: function(obj) {
			var p, str;
			str = [];
			for (p in obj) {
				if (obj.hasOwnProperty(p)) {
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				}
			}
			return str.join("&");
		},
		genAlert: function(state, text_strong, text, $put_in) {
			var html, rdm;
			if (text_strong == null) {
				text_strong = '';
			}
			rdm = Math.floor(Math.random() * 1000);
			html = '<div class="alert alert-' + state + ' alert-dismissible" id="alert' + rdm + '" role="alert"> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> <strong>' + text_strong + '</strong> ' + text + '</div>';
			setTimeout(function() {
				return $('#alert' + rdm).slideUp();
			}, 10000);
			$put_in.html(html);
			$put_in.find('.alert').slideDown('slow');
			return null;
		},
		calcularEdad: function(fecha) {
			var ahora_anio, ahora_dia, ahora_mes, anio, dia, edad, fecha_hoy, mes, values;
			values = fecha.split("-");
			dia = values[2];
			mes = values[1];
			anio = values[0];
			fecha_hoy = new Date();
			ahora_anio = fecha_hoy.getFullYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = ahora_anio - anio;
			if (ahora_mes < (mes - 1)) {
				edad--;
			}
			if (((mes - 1) === ahora_mes) && (ahora_dia < dia)) {
				edad--;
			}
			return edad;
		},
		genera_buttons: function(close, buttons) {
			var attr_data, buttonsHTML, closeHTML, i, j, _i, _len;
			if (close == null) {
				close = true;
			}
			if (buttons == null) {
				buttons = "";
			}
			closeHTML = '<a class="btn btn-default" href="#" data-dismiss="modal">Cancelar</a>';
			buttonsHTML = '';
			if (buttons !== '') {
				for (_i = 0, _len = buttons.length; _i < _len; _i++) {
					i = buttons[_i];
					attr_data = '';
					for (j in i.dataAttr) {
						attr_data += "data-" + j + "=\"" + i.dataAttr[j] + "\" ";
					}
					buttonsHTML += "<a id=\"" + i.id + "\" class=\"btn " + (i.classes.join(' ')) + "\" " + attr_data + " href=\"#\" > " + i.name + " </a>";
				}
			}
			if (close) {
				buttonsHTML += closeHTML;
			}
			return buttonsHTML;
		},
		
		overModal: function(params, fnSuccess) {
			var btns, html;
			if (fnSuccess == null) {
				fnSuccess = succesFunction;
			}
			btns = this.genera_buttons(false, params.buttons);
			if ($("#" + params.id).length) {
				$("#" + params.id).remove();
				$("#" + params.id + "_bg").remove();
			}
			html = "<div id='" + params.id + "' class='modal-dialog visible' style='z-index: 10000; position: absolute; left: 50%; margin: 0 0 0 -300px;'> <div class='modal-content'>";
			if (params.title !== void 0) {
				html += "<div class='modal-header'> <h4 class='modal-title'><strong>" + params.title + "</strong></h4> </div>";
			}
			html += "<div class='modal-body'>" + params.text + "</div> <div class='modal-footer'> " + btns + " </div> </div> </div> <div id='" + params.id + "_bg' class='modal-backdrop fade in' style='z-index: 9999;'></div>";
			
			return $('body').prepend(html);
		}
	};
}


ut = utils()