#encoding:utf-8
# Django settings for greco_socios project.

import os
import imp
import locale

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Identificando la ruta del Proyecto
PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# Identificando la ruta a Django
DJANGO_PATH = os.path.dirname(os.path.realpath(__file__))

ROOT_URLCONF = 'app.urls'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# USE_I18N and USE_L10N works in the context of Django templates and forms.
# Then we set the location for the Python's internal datetime representations.
locale.setlocale(locale.LC_ALL, '')



# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = PROJECT_PATH + '/uploads'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/uploads/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static"
    # Don't forget to use absolute paths, not relative paths.
    DJANGO_PATH + '/static',
)

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = PROJECT_PATH + '/static'

# URL prefix for static files. Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
#ADMIN_MEDIA_PREFIX = '/static/admin/'


ADMIN_ENABLED = False

# Securize site
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_SECONDS = 3600
SECURE_SSL_REDIRECT = True



# Email settings
EMAIL_USE_LOCALTIME = True
EMAIL_TIMEOUT = 60
EMAIL_SUBJECT_PREFIX = '[Greco Socios] '

# Login
LOGIN_REDIRECT_URL = "/"

MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
   
    'app.middleware.WhodidMiddleware',
)



TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'APP_DIRS': True,
    'DIRS': [ DJANGO_PATH + '/templates' ],
    'OPTIONS': {
        'context_processors': [
            
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.csrf',
            'django.template.context_processors.request',

            'app.context_processors.greco_common',
        ],
        'file_charset': 'utf-8',
    },
}]



INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'sslserver',
    'widget_tweaks',
    
    'app',
]


###########################################
## Cargamos las configuraciones del usuario
imp.load_source("user_settings", os.path.join(PROJECT_PATH, "config.py"))
from user_settings import *
