#encoding:utf-8
ITEMS = []
MENU = []


def add_item(title, url, ico, parent, order):
	global ITEMS

	ITEMS += [{
		'title': title,
		'url': url,
		'ico': ico,
		'parent': parent,
		'order': order,
	}]

	sort_menu()



def sort_menu():
	global ITEMS
	global MENU

	menu = []
	subitems = []
	
	for item in ITEMS:
		if item['parent'] is False:
			menu += [item]
		else:
			subitems += [item]

	# Remove Duplicates
	ignored_keys = ["title", "ico", "parent", "order", "sub"]
	filtered = {tuple((k, d[k]) for k in sorted(d) if k not in ignored_keys): d for d in menu}
	menu = list(filtered.values())

	# Sort by Order
	menu = sorted(menu, key=lambda k: k['order'])

	MENU = list(menu)

	for item in MENU:
		item['sub'] = []
		
		for subitem in subitems:
			if subitem['parent'] == item['url']:
				item['sub'] += [subitem]
		
		# Sort by Order
		item['sub'] = sorted(item['sub'], key=lambda k: k['order'])


add_item('Socios', '/', 'fas fa-users', False, 100)
add_item('Cobranzas', '/cobranzas/', 'fas fa-hand-holding-usd', False, 200)
