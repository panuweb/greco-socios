#encoding:utf-8
from django.template.loader import render_to_string

import tempfile

from app import context_processors

pdf_global_options = {
	'dpi': '300',
	'grayscale': '',
	'margin-top': '10mm',
	'margin-right': '10mm',
	'margin-bottom': '10mm',
	'margin-left': '10mm',
	'encoding': "UTF-8",
	'custom-header' : [ ('Accept-Encoding', 'gzip') ],
}

def formato_moneda( num ):
	return "{:,.2f}".format( num ).replace( ',', ';' ).replace( '.', ',' ).replace( ';', '.' )

def get_header_footer(a, request, context = {}):
	name = ''

	context.update(context_processors.greco_common(request))

	template = ''

	if a == 'header':
		template = 'pdf/header.html'
	
	elif a == 'footer':
		template = 'pdf/footer.html'

	
	with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as html:
		name = html.name
		html.write(
			render_to_string(template, context).encode('utf-8')
		)

	return name
