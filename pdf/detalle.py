#encoding:utf-8
#https://stackoverflow.com/questions/34479040/how-to-install-wkhtmltopdf-with-patched-qt
from django.template.loader import render_to_string
from django.http import HttpResponse

import datetime
import json
import pdfkit
import tempfile

from .commons import formato_moneda, pdf_global_options, get_header_footer
from app.models.cobranzas import Cobranza
from app.models.recibos import Recibo
from app.models.socios import Socio


from app import context_processors

def detalle_cobranza( request ):
	coza = request.GET['cobranza']
	list_coza = Cobranza.objects.get(cobranza_id=coza)

	recibos = Recibo.objects.all()
	recibos = recibos.filter(cobranza_id=coza)

	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	cobradores_files = open('tables/cobradores.json')
	cobradores_json = json.load(cobradores_files)

	entidades_cobradoras_locales_files = open('tables/entidades_cobradoras_locales.json')
	entidades_cobradoras_locales_json = json.load(entidades_cobradoras_locales_files)

	categorias_data = {}

	for c in categorias_json:
		categorias_data[ c ] = { 
			'mount': 0,
			'count': 0,
			'name': categorias_json[ c ]['texto']
		}


	cobradores_dom_data = {}

	for c in cobradores_json:
		cobradores_dom_data[ c ] = { 
			'mount': 0,
			'count': 0,
			'name': cobradores_json[ c ]['nombre']
		}

	tipo_cobranza_elec = Socio.cobranza_epago_sistema_choices
	tipo_cobranza_elec_data = {}
	entidades_locales = entidades_cobradoras_locales_json

	for c in tipo_cobranza_elec:
		tipo_cobranza_elec_data[ c ] = { 
			'mount': 0,
			'count': 0,
			'name': tipo_cobranza_elec[ c ]
		}

		if 'a' == c or 'd' == c:
			tipo_cobranza_elec_data[ c ]['entidades'] = {}

			for e in entidades_locales:
				tipo_cobranza_elec_data[ c ]['entidades'][ e ] = {
					'mount': 0,
					'count': 0,
					'name': entidades_locales[ e ]['nombre']
				}

	global_mount = 0
	biblioteca_mount = 0
	biblioteca_count = 0
	domicilio_mount = 0
	domicilio_count = 0
	electronica_mount = 0
	electronica_count = 0

	for i in recibos:
		global_mount = global_mount + float( i.cuota )

		if 'b' == i.cobranza_tipo:
			biblioteca_mount += float( i.cuota )
			biblioteca_count += 1

		if 'd' == i.cobranza_tipo:
			domicilio_mount += float( i.cuota )
			domicilio_count += 1

		if 'e' == i.cobranza_tipo:
			electronica_mount += float( i.cuota )
			electronica_count += 1

		categorias_data[ str( i.categoria_id ) ]['mount'] += float( i.cuota )
		categorias_data[ str( i.categoria_id ) ]['count'] += 1

		if 'd' == i.cobranza_tipo and None != i.cobrador_id:
			cobradores_dom_data[ str( i.cobrador_id ) ]['mount'] += float( i.cuota )
			cobradores_dom_data[ str( i.cobrador_id ) ]['count'] += 1

		if 'e' == i.cobranza_tipo and None != i.epago_sistema and '0' != i.epago_sistema:
			tipo_cobranza_elec_data[ i.epago_sistema ]['mount'] += float( i.cuota )
			tipo_cobranza_elec_data[ i.epago_sistema ]['count'] += 1

			if 'a' == i.epago_sistema or 'd' == i.epago_sistema:
				tipo_cobranza_elec_data[ i.epago_sistema ]['entidades'][ str( i.epago_entidad_local_id ) ]['mount'] +=  float( i.cuota )
				tipo_cobranza_elec_data[ i.epago_sistema ]['entidades'][ str( i.epago_entidad_local_id ) ]['count'] += 1 

	for c in categorias_json:
		categorias_data[ c ]['mount'] = formato_moneda( categorias_data[ c ]['mount'] )

	for c in cobradores_json:
		cobradores_dom_data[ c ]['mount'] = formato_moneda( cobradores_dom_data[ c ]['mount'] )

		if 0 == cobradores_dom_data[ c ]['count']:
			del cobradores_dom_data[ c ]

	for c in tipo_cobranza_elec:
		tipo_cobranza_elec_data[ c ]['mount'] = formato_moneda( tipo_cobranza_elec_data[ c ]['mount'] )

		if 'entidades' in tipo_cobranza_elec_data[ c ]:
			tipo_cobranza_elec_data[ c ]['entidades'] = tipo_cobranza_elec_data[ c ]['entidades'].values()
			
		if 0 == tipo_cobranza_elec_data[ c ]['count']:
			del tipo_cobranza_elec_data[ c ]

	cobranza_data = list_coza

	if '' == cobranza_data.notas:
		cobranza_data.notas = False

	cobranza_estado_translate = {
		"new": "Nueva",
		"init": "Iniciada",
		"end": "Finalizada",
		"cancel": "Cancelada"
	}

	cobranza_data.estado = cobranza_estado_translate[ cobranza_data.estado ]

	cobranza_data.stats = {
		'global': {
			'mount': formato_moneda( global_mount ),
			'count': recibos.count,
		},
		'biblioteca': {
			'mount': formato_moneda( biblioteca_mount ),
			'count': biblioteca_count,
		},
		'domicilio': {
			'mount': formato_moneda( domicilio_mount ),
			'count': domicilio_count,
		},
		'electronica': {
			'mount': formato_moneda( electronica_mount ),
			'count': electronica_count,
		},
		'categorias': categorias_data.values(),
		'cobradores_dom': cobradores_dom_data.values(),
		'cobradores_elec': tipo_cobranza_elec_data.values(),
	}

	print cobranza_data.notas

	context = {
		'cobranza': cobranza_data,
		'current_date': datetime.datetime.now(),
	}

	context.update(context_processors.greco_common(request))
	html = render_to_string('pdf/cobranza_detalle.html', context)

	header_options = {
		'reporte_id': 'detalle_cobranza',
		'reporte_nombre': 'Detalle de cobranza',
	}

	pdf_options = pdf_global_options.copy()
	pdf_options.update({
		'margin-top': '35mm',
		'margin-bottom': '15mm',

		'header-html': get_header_footer('header', request, header_options), 
		'footer-html': get_header_footer('footer', request), 
	})

	pdf = pdfkit.from_string(html, False, options=pdf_options)
	response = HttpResponse(pdf, content_type='application/pdf')

	return response
