#encoding:utf-8
from django.http import HttpResponse
from django.http import JsonResponse
from django.template.loader import render_to_string

from django.db.models import Q
from itertools import chain
import pdfkit
import json

from .commons import formato_moneda, pdf_global_options, get_header_footer

from app.models.cobranzas import Cobranza
from app.models.recibos import Recibo
from app.models.socios import Socio

from app import context_processors



#@login_required(login_url='/login.html')
def recibos_cobranza(request):
	filtro_cobranza = request.GET['cobranza']

	recibos = Recibo.objects.all()
	recibos = recibos.filter(cobranza_id=filtro_cobranza)

	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	localidades_files = open('tables/localidades.json')
	localidades_json = json.load(localidades_files)

	cobradores_files = open('tables/cobradores.json')
	cobradores_json = json.load(cobradores_files)

	# Recibos Biblioteca
	recibos_bib = recibos.filter( Q(cobranza_tipo='b') )
	recibos_bib = recibos_bib.order_by('cobranza_tipo','socio__num_socio')

	# Recibos Cobradores (Todos)
	recibos_cob = recibos.filter(Q(cobranza_tipo='d'))
	recibos_cob = recibos_cob.order_by('cobrador_id', 'socio__num_socio')

	# Recibos Debito (Check SQL >> SELECT * FROM `socios_socio` WHERE cobranza_tipo = 'e' AND cobranza_epago_sistema = 'd' AND status = 'active' AND baja_fecha IS NULL ORDER BY `cobranza_epago_sistema` DESC$
	recibos_deb = recibos.filter( cobranza_tipo='e', epago_sistema='d' )
	recibos_deb = recibos_deb.order_by('socio__apellido', 'socio__nombre')

	recibos_join = list(chain(recibos_bib, recibos_cob, recibos_deb))

	error = False
	error_msj = ''

	for i in recibos_join:
		catego = categorias_json[ str( i.categoria_id ) ]
		i.categoria = catego['categoria'] + ' - ' + catego['texto']

		localidad = localidades_json[ str( i.socio.domicilio_localidad_id ) ]
		i.socio.domicilio_localidad = localidad['nombre']

		if i.cobrador_id is not None:
			cobrador = cobradores_json[ str( i.cobrador_id ) ]
			i.cobrador = cobrador['nombre']

		else:
			i.cobrador = ''

		


	context = {'recibos': recibos_join}
	context.update(context_processors.greco_common(request))

	if error:
		html = error_msj
	else:
		html = render_to_string('pdf/recibos.html', context)

	pdf_options = pdf_global_options.copy()

	pdf = pdfkit.from_string(html, False, options=pdf_options)
	response = HttpResponse(pdf, content_type='application/pdf')

	return response
