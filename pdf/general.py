#encoding:utf-8

#@login_required(login_url='/login.html')
def listado_bajas(request):
	desde = request.GET.get('desde','1900-01-01')
	hasta = request.GET.get('hasta', '2099-12-31')
	orde  = request.GET.get('orden', 'baja')

	if desde == '':
		desde = '1900-01-01'

	if hasta == '':
		hasta = '2099-12-31'
	
	socios = Socio.objects
	socios = socios.filter(Q(baja_fecha__gte=desde) & Q(baja_fecha__lte=hasta))
	socios = socios.only("num_socio", "baja_fecha", "apellido", "nombre")

	if orde == 'num':
		socios = socios.order_by('num_socio')
		orden_txt = 'Numero de Socio'
	
	elif orde == 'ape':
		socios = socios.order_by('apellido','nombre')
		orden_txt = 'Apellido y Nombre'
	
	elif orde == 'baja':
		socios = socios.order_by('baja_fecha','apellido','nombre')
		orden_txt = 'Fecha de Baja'


	context = { 'socios': socios }
	context.update(context_processors.greco_common(request))
	html = render_to_string('reportes/pdf/listado_bajas.html', context)

	header_options = {
		'orden_txt': orden_txt,
		'fecha_desde': datetime.datetime.strptime(desde, "%Y-%m-%d").date().strftime('%d-%m-%Y'),
		'fecha_hasta': datetime.datetime.strptime(hasta, "%Y-%m-%d").date().strftime('%d-%m-%Y'),
		'reporte_nombre': 'Listado de Socios dados de Baja',
		'reporte_id': 'listado_bajas',
	}

	pdf_options = pdf_global_options.copy()
	pdf_options.update({
		'margin-top': '35mm',
		'margin-bottom': '15mm',

		'header-html': get_header_footer('header', request, header_options), 
		'footer-html': get_header_footer('footer', request), 
	})

	pdf = pdfkit.from_string(html, False, options=pdf_options)
	response = HttpResponse(pdf, content_type='application/pdf')

	return response
