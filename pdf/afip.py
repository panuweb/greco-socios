#encoding:utf-8
from django.http import HttpResponse
from django.template.loader import render_to_string

import pdfkit

from .commons import formato_moneda, pdf_global_options, get_header_footer

from app.models.cobranzas import Cobranza
from app.models.recibos import Recibo

from app import context_processors

def cobranza_recibos_afip(request):
	coza = request.GET['cobranza']

	recibos_afip = Recibo.objects.all()
	recibos_afip = recibos_afip.filter(cobranza_id=coza,recibo_afip=True)

	coza = Cobranza.objects.get(cobranza_id=coza)


	context = {
		'cobranza': coza,
		'recibos_afip': recibos_afip,
	}

	context.update(context_processors.greco_common(request))
	html = render_to_string('pdf/cobranza_recibos_afip.html', context)

	header_options = {
		'reporte_id': 'detalle_cobranza_afip',
		'reporte_nombre': 'Recibos AFIP',
	}

	pdf_options = pdf_global_options.copy()
	pdf_options.update({
		'margin-top': '35mm',
		'margin-bottom': '15mm',

		'header-html': get_header_footer('header', request, header_options), 
		'footer-html': get_header_footer('footer', request), 
	})

	pdf = pdfkit.from_string(html, False, options=pdf_options)
	response = HttpResponse(pdf, content_type='application/pdf')

	return response
