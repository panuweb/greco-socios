#encoding:utf-8

from django.template.loader import render_to_string
from django.shortcuts import render
from django.http import HttpResponse

from datetime import datetime
from dateutil.relativedelta import relativedelta  


from .commons import formato_moneda, pdf_global_options, get_header_footer
from app.models.socios import Socio

from app import context_processors
import json
import pdfkit

def listado_cobradores(request):
	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	cobradores_files = open('tables/cobradores.json')
	cobradores_json = json.load(cobradores_files)

	socios = Socio.objects.all()
	socios = socios.filter(status='active')
	socios = socios.filter(baja_fecha=None)
	socios = socios.filter(cobranza_tipo='d')
	socios = socios.order_by( 'cobranza_cobrador_id','num_socio' )

	for i in socios:
		i.categoria = categorias_json[ str( i.categoria_id ) ]['texto']
		i.cobrador = cobradores_json[ str( i.cobranza_cobrador_id ) ]['nombre']


	context = { 'socios': socios }
	context.update(context_processors.greco_common(request))
	html = render_to_string('pdf/listado_cobradores.html', context)


	header_options = {
		'reporte_id': 'listado_cobradores',
		'reporte_nombre': 'Listado de Socios por Cobrador',
	}

	pdf_options = pdf_global_options.copy()
	pdf_options.update({
		'margin-top': '35mm',
		'margin-bottom': '15mm',

		'header-html': get_header_footer('header', request, header_options), 
		'footer-html': get_header_footer('footer', request), 
	})

	pdf = pdfkit.from_string(html, False, options=pdf_options)
	response = HttpResponse(pdf, content_type='application/pdf')

	return response

def listado_asamblea(request):
	fecha_asamblea = datetime.strptime(request.GET['fecha_asamblea'], "%Y-%m-%d")
	fecha_ingreso_habilitados = fecha_asamblea - relativedelta( months=6 )
	fecha_edad_habilitados = fecha_asamblea - relativedelta( years=18 )

	socios = Socio.objects.all()
	socios = socios.filter(status='active')
	socios = socios.filter(baja_fecha=None)
	socios = socios.filter(alta_fecha__lt=fecha_ingreso_habilitados)
	socios = socios.filter(fecha_nacim__lt=fecha_edad_habilitados)
	socios = socios.order_by( 'apellido','nombre' )

	total_socio_habilitados = socios.count()

	context = {
		'socios': socios,
		'fecha_asamblea': fecha_asamblea,
		'fecha_ingreso_habilitados': fecha_ingreso_habilitados,
		'fecha_edad_habilitados': fecha_edad_habilitados,
		'total_socio_habilitados': total_socio_habilitados,
	}

	context.update(context_processors.greco_common(request))
	html = render_to_string('pdf/listado_asamblea.html', context)

	header_options = {
		'reporte_id': 'listado_asamblea',
		'reporte_nombre': 'Listado de Socios con derecho a voto en Asamblea',
		'fecha_asamblea': fecha_asamblea,
	}

	pdf_options = pdf_global_options.copy()
	pdf_options.update({
		'margin-top': '35mm',
		'margin-bottom': '15mm',

		'header-html': get_header_footer('header', request, header_options), 
		'footer-html': get_header_footer('footer', request), 
	})

	pdf = pdfkit.from_string(html, False, options=pdf_options)


	response = HttpResponse(pdf, content_type='application/pdf')
	#response = HttpResponse(html)
	return response