#encoding:utf-8
from datetime import date, datetime

from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Max
from django.db.models import Q
from django.forms.models import model_to_dict

from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseForbidden

import json
import urllib

from app.models.socios import Socio

def f_comma(my_str, group=3, char=','):
	"""
	Formatea cadenas agregando un caracter como separador de grupos.
	Comienza desde la derecha hacia la izquierda
	"""
	my_str = str(my_str)
	my_str = ''.join(reversed(my_str))

	edited = char.join(my_str[i:i+group] for i in range(0, len(my_str), group))

	return ''.join(reversed(edited))



def socios_list(request):
	'''Genera JSON con la lista de socios'''
	
	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	socios = Socio.objects.all()
	
	socios = socios.filter(status='active')

	baja = request.GET.get('b')

	if baja != '1':
		socios = socios.filter(baja_fecha=None)

	data = {
		'data': []
	}

	for i in socios:
		baja_value = ''
		row_class = ''

		if i.baja_fecha is not None:
			baja_value = i.baja_fecha.strftime("%d-%m-%Y")
			row_class = 'table-danger'

		data['data'] += [{
			'DT_RowId': i.socio_id,
			'num_socio': i.num_socio,
			'apellido': i.apellido,
			'nombre': i.nombre,
			'categoria': categorias_json[ str( i.categoria_id ) ]['texto'],
			'tipo_cobranza': '',
			'tipo_cobranza': i.get_cobranza_tipo_display(),
			'baja': baja_value,
			'actions': '<a href="#" class="btn btn-link"><i class="far fa-eye"></i></a>',
			'DT_RowClass': row_class,
		}]

	return JsonResponse(data)



def socios_details(request):
	'''Genera JSON con los datos del socio especificado'''

	localidades_files = open('tables/localidades.json')
	localidades_json = json.load(localidades_files)

	provincias_files = open('tables/provincias.json')
	provincias_json = json.load(provincias_files)

	cobradores_files = open('tables/cobradores.json')
	cobradores_json = json.load(cobradores_files)

	entidades_cobradoras_locales_files = open('tables/entidades_cobradoras_locales.json')
	entidades_cobradoras_locales_json = json.load(entidades_cobradoras_locales_files)

	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	socio_id = request.GET.get('socio_id')

	if socio_id is None:
		return JsonResponse({})

	selected_socio = Socio.objects.filter(socio_id=socio_id)[0]

	data = model_to_dict(selected_socio)

	# Formateamos campos
	data['documento'] = f_comma(data['documento'], group=3, char='.')
	data['fecha_nacim'] = data['fecha_nacim'].strftime('%d/%m/%Y')

	# Creamos la cadena domicilio
	data['domicilio'] = ''
	data['domicilio'] += selected_socio.domicilio_calle

	if selected_socio.domicilio_calle != '':
		data['domicilio'] += ' ' + str(selected_socio.domicilio_altura)

	if selected_socio.domicilio_piso_dpto != '':
		data['domicilio'] += ' ' + selected_socio.domicilio_piso_dpto

	if selected_socio.domicilio_aclaraciones != '':
		data['domicilio'] += ' [' + selected_socio.domicilio_aclaraciones + ']'

	data['domicilio'] += ','

	if selected_socio.domicilio_cod_postal != '':
		data['domicilio'] += ' (' + selected_socio.domicilio_cod_postal + ')'

	data['domicilio'] += ' '

	localidad = localidades_json[ str( selected_socio.domicilio_localidad_id ) ]
	localidad_nombre = localidad['nombre']
	provincia = provincias_json[ str( localidad['provincia_id'] ) ]
	provincia_nombre = provincia['nombre']

	data['domicilio'] += localidad_nombre + ', ' + provincia_nombre

	# Convertimos el string JSON en un objeto iterable
	#data['autorizados'] = json.loads(data['autorizados'])
	#data['convenios'] = json.loads(data['convenios'])

	data['cobranza_tipo_name'] = Socio.cobranza_tipo_choices[selected_socio.cobranza_tipo]
	data['cobranza_tipo_is_biblioteca'] = bool(data['cobranza_tipo'] == 'b')
	data['cobranza_tipo_is_domicilio'] = bool(data['cobranza_tipo'] == 'd')
	data['cobranza_tipo_is_electronico'] = bool(data['cobranza_tipo'] == 'e')

	if data['cobranza_tipo_is_domicilio']:
		cobrador = cobradores_json[ str( selected_socio.cobranza_cobrador_id ) ]
		cobrador_nombre = cobrador['nombre']

		data['cobranza_cobrador_name'] = cobrador_nombre
		data['cobranza_cobrador_domicilio_especial'] = bool(
			data['cobranza_cobrador_domicilio_especial'] == '1'
		)

	if data['cobranza_tipo_is_electronico']:
		data['cobranza_epago_sistema_name'] = Socio.cobranza_epago_sistema_choices[selected_socio.cobranza_epago_sistema]
		data['cobranza_epago_sistema_is_automatico_local'] = bool(data['cobranza_epago_sistema'] == 'a')
		data['cobranza_epago_sistema_is_automatico_nacional'] = bool(data['cobranza_epago_sistema'] == 'b')
		data['cobranza_epago_sistema_is_directo_nacional'] = bool(data['cobranza_epago_sistema'] == 'c')
		data['cobranza_epago_sistema_is_directo_local'] = bool(data['cobranza_epago_sistema'] == 'd')

		data['cobranza_epago_tarjeta_numero'] = f_comma(data['cobranza_epago_tarjeta_numero'], group=4, char=' ')
		data['cobranza_epago_tarjeta_vencimiento'] = f_comma(data['cobranza_epago_tarjeta_vencimiento'], group=2, char='/')

		if selected_socio.cobranza_epago_entidad_local_id is not None:

			entidad_cobrador_local = entidades_cobradoras_locales_json[ str( selected_socio.cobranza_epago_entidad_local_id ) ]
			entidad_cobrador_local_nombre = entidad_cobrador_local['nombre']

			data['cobranza_epago_entidad_local'] = entidad_cobrador_local_nombre

		else:
			data['cobranza_epago_entidad_local'] = ''

	data['cobranza_titular_documento'] = f_comma(data['cobranza_titular_documento'], group=3, char='.')

	categoria = categorias_json[ str( selected_socio.categoria_id ) ]
	categoria_texto = categoria['texto']

	data['categoria'] = categoria_texto

	data['alta_fecha'] = data['alta_fecha'].strftime('%d/%m/%Y')
	data['is_baja'] = False

	if selected_socio.baja_fecha is not None:
		data['baja_fecha'] = data['baja_fecha'].strftime('%d/%m/%Y')
		data['is_baja'] = True

	data['creacion_fecha'] = data['creacion_fecha'].strftime('%d/%m/%Y %H:%M:%S')

	#data['creacion_usuario'] = selected_socio.creacion_usuario.first_name + ' ' + selected_socio.creacion_usuario.last_name

	return JsonResponse( data )



def socios_baja(request):
	'''Genera JSON con los datos del socio especificado'''
	socio_id = request.GET.get('socio_id')

	if socio_id is None:
		return JsonResponse({})

	selected_socio = Socio.objects.filter(socio_id=socio_id)[0]
	data = model_to_dict(selected_socio)

	data['documento'] = f_comma(data['documento'], group=3, char='.')
	data['baja_fecha'] = date.today()

	return JsonResponse(data)

def socios_confirm_baja(request):
	'''Genera JSON con los datos del socio especificado'''
	input_data = json.loads(request.GET.get('data'))
	socio_id = int(input_data['socio_id'])

	print 'Baja socio'
	socio_history = Socio.objects.get(socio_id=socio_id)
	socio_edited = Socio.objects.get(socio_id=socio_id)

	socio_edited.baja_fecha = input_data['baja_fecha']
	#socio_edited.baja_data = input_data['baja_data']
	socio_edited.baja_data = ''

	# System data
	socio_edited.creacion_fecha = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

	socio_edited.save()

	# Guardamos el registro histórico
	socio_history.socio_id = None
	socio_history.status = 'history'

	socio_history.save()
	
	data = {}

	data['exito'] = True
	data['deleted'] = True
	data['nombre'] = socio_edited.nombre + ' ' + socio_edited.apellido;
	data['numero'] = str( socio_edited.num_socio );

	return JsonResponse(data)

def socios_digibepe(request):
	'''Genera JSON con la lista de socios'''
	
	current_time = datetime.now()

	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	localidades_files = open('tables/localidades.json')
	localidades_json = json.load(localidades_files)

	provincias_files = open('tables/provincias.json')
	provincias_json = json.load(provincias_files)

	socios = Socio.objects.all().order_by('num_socio')
	
	socios = socios.filter(status='active')


	data = {
		'data': {},
		'ahora': current_time.strftime( '%y-%m-%d_%H-%M-%S' )
	}

	headers = ''

	# Cabeceras
	headers += 'branchcode'
	headers += ',cardnumber'
	headers += ',surname'
	headers += ',firstname'
	headers += ',dateofbirth'
	headers += ',streetnumber'
	headers += ',address'
	headers += ',city'
	headers += ',state'
	headers += ',zipcode'
	headers += ',phone'
	headers += ',email'
	headers += ',categorycode'
	headers += ',dateenrolled'
	headers += ',dateexpiry'
	headers += ',borrowernotes'
	headers += '\n'

	info = ''
	count_socios = 0
	count_files = 1

	for i in socios:

		if count_socios == 1500:
			with_headers = headers + info
			data['data'][ count_files ] = urllib.quote( with_headers.encode( "utf-8" ) )
			info = ''
			count_socios = 0
			count_files = count_files + 1

		baja_value = ''
		row_class = ''

		if i.baja_fecha is None:
			category = categorias_json[ str( i.categoria_id ) ]['categoria']
			fecha_baja = '2100-12-31'

		else:
			category = 'Z'
			fecha_baja = str( i.baja_fecha )

		if i.domicilio_localidad_id is not None:
			localidad = localidades_json[ str( i.domicilio_localidad_id ) ]
			localidad_nombre = localidad['nombre']
			
			provincia = provincias_json[ str( localidad['provincia_id'] ) ]
			provincia_nombre = provincia['nombre']

		else: 
			localidad_nombre = ''
			provincia_nombre = ''

		notas = ''

		if i.cobranza_tipo == 'b':
			notas += "<br><br><span style='color: #8bc34a;'>Este socio paga la cuota en la BIBLIOTECA, comprobar que no adeude cuotas antes de prestar material.</span>"

		notas += "<br><br><small style='color: #2196f3;'>&Uacute;ltima actualizaci&oacute;n de socios: " + current_time.strftime( '%d-%m-%Y %H:%M' ) + "</small>"

		info += '"0461"'
		info += ',"' + str( i.num_socio ).zfill(4) + '"'
		info += ',"' + i.apellido + '"'
		info += ',"' + i.nombre + '"'
		info += ',"' + str( i.fecha_nacim ) + '"'
		info += ',"' + str( i.domicilio_altura ) + ' ' + i.domicilio_piso_dpto + '"'
		info += ',"' + i.domicilio_calle + '"'
		info += ',"' + localidad_nombre + '"'
		info += ',"' + provincia_nombre + '"'
		info += ',"' + i.domicilio_cod_postal + '"'
		info += ',"' + i.contacto_telefono + '"'
		info += ',"' + i.contacto_email + '"'
		info += ',"' + category + '"'
		info += ',"' + str( i.alta_fecha ) + '"'
		info += ',"' + fecha_baja + '"'
		info += ',"' + notas + '"'
		info += '\n'

		count_socios = count_socios + 1

	# Resto de los socios
	with_headers = headers + info
	data['data'][ count_files ] = urllib.quote( with_headers.encode( "utf-8" ) )

	return JsonResponse( data )
