#encoding:utf-8
from datetime import date, datetime

from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Max
from django.db.models import Q
from django.forms.models import model_to_dict

from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseForbidden

import json

from app.models.socios import Socio

def form(request):
	'''Genera la informacion necesaria para el formulario de nuevo/editar socio'''

	localidades_files = open('tables/localidades.json')
	localidades_json = json.load(localidades_files)

	provincias_files = open('tables/provincias.json')
	provincias_json = json.load(provincias_files)

	cobradores_files = open('tables/cobradores.json')
	cobradores_json = json.load(cobradores_files)

	filtered_cobradores = {}

	for cobrador in cobradores_json:
		if '' == cobradores_json[ cobrador ]['baja']:
			filtered_cobradores[ cobrador ] = cobradores_json[ cobrador ]

	cobradores_json = filtered_cobradores

	entidades_cobradoras_locales_files = open('tables/entidades_cobradoras_locales.json')
	entidades_cobradoras_locales_json = json.load(entidades_cobradoras_locales_files)

	categorias_files = open('tables/categorias.json')
	categorias_json = json.load(categorias_files)

	convenios_files = open('tables/convenios.json')
	convenios_json = json.load(convenios_files)

	socio_id = request.GET.get('socio_id')
	form_data = request.GET.get('data')

	if socio_id == "false":
		'''Valores por defecto para socios nuevos'''
		socio_data = {
			'domicilio_provincia_id': 1,
			'domicilio_localidad_id': 1,
			'domicilio_cod_postal': '3013',
			'cobranza_tipo': 'e',
			'cobranza_epago_sistema': 'a',
			'alta_fecha': date.today(),
			'categoria_id': 2,
		}

	else:
		selected_socio = Socio.objects.filter(socio_id=socio_id)[0]

		socio_data = model_to_dict(selected_socio)

		# Extra data
		localidad = localidades_json[ str( socio_data['domicilio_localidad_id'] ) ]
		socio_data['domicilio_provincia_id'] = localidad['provincia_id']

		# Convertimos el string JSON en un objeto iterable
		#socio_data['autorizados'] = json.loads(socio_data['autorizados'])
		
		socio_data['convenios'] = convenios_json

		'''
		for convenio_socio in socio_data['convenios']:
			for convenio in convenios:
				if str(convenio_socio['convenio_id']) == str(convenio['convenio_id']):
					convenio['selected'] = True

					convenio['datos_socio'] = convenio_socio['datos']
		'''

	data = {
		'provincias': provincias_json,
		'localidades': localidades_json,
		'convenios': convenios_json,
		'formas_pago': Socio.cobranza_tipo_choices,
		'sistema_epago': Socio.cobranza_epago_sistema_choices,
		'cobradores': cobradores_json,
		'entidades_locales': entidades_cobradoras_locales_json,
		'categorias': categorias_json,
		'socio': socio_data,
	}


	if None != form_data:
		data['socio'] = json.loads( form_data )

	return JsonResponse(data)


def save(request):
	'''Recibe los datos del formulario nuevo/editar socio y actualiza los datos en la bbdd'''

	entidades_cobradoras_locales_files = open('tables/entidades_cobradoras_locales.json')
	entidades_cobradoras_locales_json = json.load(entidades_cobradoras_locales_files)

	data = {
		'info': {},
	}

	input_data = json.loads(request.GET.get('data'))
	socio_id = int(input_data['socio_id'])

	# Get next num_socio
	num_socio = Socio.objects.all().aggregate(Max('num_socio'))['num_socio__max'] + 1

	#print input_data
	print input_data[ 'socio_id' ]

	if socio_id == 0:
		# Nuevo socio.
		print 'Nuevo socio'

		entidad_local = input_data['cobranza_epago_entidad_local_id']

		if input_data['categoria_protector_cuotas_adicionales'] == '':
			input_data['categoria_protector_cuotas_adicionales'] = 0

		if input_data['cobranza_monto_especial'] is None:
			input_data['cobranza_monto_especial'] = 0

		if input_data['cobranza_cobrador_id'] == 0 or input_data['cobranza_cobrador_id'] == '':
			input_data['cobranza_cobrador_id'] = None

		socio_created = Socio(
			apellido=input_data['apellido'],
			nombre=input_data['nombre'],
			documento=input_data['documento'],
			fecha_nacim=input_data['fecha_nacim'],

			domicilio_calle=input_data['domicilio_calle'],
			domicilio_altura=input_data['domicilio_altura'],
			domicilio_piso_dpto=input_data['domicilio_piso_dpto'],
			domicilio_aclaraciones=input_data['domicilio_aclaraciones'],
			domicilio_cod_postal=input_data['domicilio_cod_postal'],
			domicilio_localidad_id=input_data['domicilio_localidad_id'],

			contacto_email=input_data['contacto_email'],
			contacto_telefono=input_data['contacto_telefono'],
			contacto_otro=input_data['contacto_otro'],

			#autorizados=json.dumps(input_data['autorizados']),
			autorizados=json.dumps([]),
			#convenios=json.dumps(input_data['convenios']),
			convenios=json.dumps([]),

			cobranza_tipo=input_data['cobranza_tipo'],
			cobranza_monto_especial=input_data['cobranza_monto_especial'],
			cobranza_biblioteca_motivo=input_data['cobranza_biblioteca_motivo'],
			cobranza_cobrador_id=input_data['cobranza_cobrador_id'],
			cobranza_cobrador_domicilio_especial=input_data['cobranza_cobrador_domicilio_especial'],
			cobranza_cobrador_domicilio=input_data['cobranza_cobrador_domicilio'],
			cobranza_cobrador_dias_horas=input_data['cobranza_cobrador_dias_horas'],
			cobranza_cobrador_otros=input_data['cobranza_cobrador_otros'],
			cobranza_epago_sistema=input_data['cobranza_epago_sistema'],
			#cobranza_epago_entidad_nacional=input_data['cobranza_epago_entidad_nacional'],
			cobranza_epago_entidad_nacional='',
			cobranza_epago_entidad_local_id=entidad_local,
			#cobranza_epago_tarjeta_marca=input_data['cobranza_epago_tarjeta_marca'],
			cobranza_epago_tarjeta_marca='',
			#cobranza_epago_tarjeta_numero=input_data['cobranza_epago_tarjeta_numero'],
			cobranza_epago_tarjeta_numero='',
			#cobranza_epago_tarjeta_vencimiento=input_data['cobranza_epago_tarjeta_vencimiento'],
			cobranza_epago_tarjeta_vencimiento='',
			cobranza_epago_cuenta_numero=input_data['cobranza_epago_cuenta_numero'],
			#cobranza_epago_cuenta_cbu=input_data['cobranza_epago_cuenta_cbu'],
			cobranza_epago_cuenta_cbu='',
			#cobranza_titular_nombre=input_data['cobranza_titular_nombre'],
			cobranza_titular_nombre='',
			#cobranza_titular_documento=input_data['cobranza_titular_documento'],
			cobranza_titular_documento='',
			cobranza_recibo_afip=input_data['cobranza_recibo_afip'],

			categoria_id=input_data['categoria_id'],
			categoria_protector_cuotas_adicionales=input_data['categoria_protector_cuotas_adicionales'],
			alta_fecha=input_data['alta_fecha'],
			#alta_data=input_data['alta_data'],
			alta_data='',

			#observaciones=input_data['observaciones'],
			observaciones='',

			# System data
			creacion_fecha=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
			status='active',

			socio_id=None,
			num_socio=num_socio,
			socio_id_original=0,
			)

		socio_created.save()
		
		socio_created.socio_id_original = socio_created.socio_id
		socio_created.save()

		data['exito'] = True
		data['created'] = True
		data['nombre'] = socio_created.nombre + ' ' + socio_created.apellido;
		data['numero'] = str( socio_created.num_socio );

	else:
		# Editar socio.
		print 'Editar socio'
		socio_history = Socio.objects.get(socio_id=socio_id)
		socio_edited = Socio.objects.get(socio_id=socio_id)

		socio_edited.apellido = input_data['apellido']
		socio_edited.nombre = input_data['nombre']
		socio_edited.documento = input_data['documento']
		socio_edited.fecha_nacim = input_data['fecha_nacim']
		
		socio_edited.domicilio_calle = input_data['domicilio_calle']
		socio_edited.domicilio_altura = input_data['domicilio_altura']
		socio_edited.domicilio_piso_dpto = input_data['domicilio_piso_dpto']
		socio_edited.domicilio_aclaraciones = input_data['domicilio_aclaraciones']
		socio_edited.domicilio_cod_postal = input_data['domicilio_cod_postal']
		socio_edited.domicilio_localidad_id = input_data['domicilio_localidad_id']

		socio_edited.contacto_email = input_data['contacto_email']
		socio_edited.contacto_telefono = input_data['contacto_telefono']
		socio_edited.contacto_otro = input_data['contacto_otro']

		#socio_edited.autorizados = json.dumps(input_data['autorizados'])
		socio_edited.autorizados = json.dumps([])
		#socio_edited.convenios = json.dumps(input_data['convenios'])
		socio_edited.convenios = json.dumps([])

		socio_edited.cobranza_tipo = input_data['cobranza_tipo']
		#socio_edited.cobranza_monto_especial = input_data['cobranza_monto_especial']
		socio_edited.cobranza_biblioteca_motivo = input_data['cobranza_biblioteca_motivo']

		cobrador = None

		if input_data['cobranza_cobrador_id'] != '0' and input_data['cobranza_cobrador_id'] != '':
			cobrador = input_data['cobranza_cobrador_id']
		
		socio_edited.cobranza_cobrador_id = cobrador
		socio_edited.cobranza_cobrador_domicilio_especial = input_data['cobranza_cobrador_domicilio_especial']
		socio_edited.cobranza_cobrador_domicilio = input_data['cobranza_cobrador_domicilio']
		socio_edited.cobranza_cobrador_dias_horas = input_data['cobranza_cobrador_dias_horas']
		socio_edited.cobranza_cobrador_otros = input_data['cobranza_cobrador_otros']
		socio_edited.cobranza_epago_sistema = input_data['cobranza_epago_sistema']
		#socio_edited.cobranza_epago_entidad_nacional = input_data['cobranza_epago_entidad_nacional']
		socio_edited.cobranza_epago_entidad_nacional = ''

		socio_edited.cobranza_epago_entidad_local_id = None

		if input_data['cobranza_epago_entidad_local_id'] != '' and input_data['cobranza_epago_entidad_local_id'] != '0' :
			socio_edited.cobranza_epago_entidad_local_id = str( input_data['cobranza_epago_entidad_local_id'] )

		#socio_edited.cobranza_epago_tarjeta_marca = input_data['cobranza_epago_tarjeta_marca']
		socio_edited.cobranza_epago_tarjeta_marca = ''
		#socio_edited.cobranza_epago_tarjeta_numero = input_data['cobranza_epago_tarjeta_numero']
		socio_edited.cobranza_epago_tarjeta_numero = ''
		#socio_edited.cobranza_epago_tarjeta_vencimiento = input_data['cobranza_epago_tarjeta_vencimiento']
		socio_edited.cobranza_epago_tarjeta_vencimiento = ''
		socio_edited.cobranza_epago_cuenta_numero = input_data['cobranza_epago_cuenta_numero']
		#socio_edited.cobranza_epago_cuenta_cbu = input_data['cobranza_epago_cuenta_cbu']
		socio_edited.cobranza_epago_cuenta_cbu = ''
		#socio_edited.cobranza_titular_nombre = input_data['cobranza_titular_nombre']
		socio_edited.cobranza_titular_nombre = ''
		#socio_edited.cobranza_titular_documento = input_data['cobranza_titular_documento']
		socio_edited.cobranza_titular_documento = ''

		socio_edited.cobranza_recibo_afip = input_data['cobranza_recibo_afip']

		socio_edited.categoria_id = input_data['categoria_id']
		socio_edited.categoria_protector_cuotas_adicionales=input_data['categoria_protector_cuotas_adicionales']
		socio_edited.alta_fecha = input_data['alta_fecha']
		#socio_edited.alta_data = input_data['alta_data']
		socio_edited.alta_data = ''

		#socio_edited.observaciones = input_data['observaciones']
		socio_edited.observaciones = ''

		# System data
		socio_edited.creacion_fecha = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

		socio_edited.save()


		# Guardamos el registro histórico
		socio_history.socio_id = None
		socio_history.status = 'history'

		socio_history.save()

		data['exito'] = True
		data['edited'] = True
		data['nombre'] = socio_edited.nombre + ' ' + socio_edited.apellido
		data['numero'] = str( socio_edited.num_socio )

	return JsonResponse(data)
