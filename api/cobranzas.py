#encoding:utf-8
from django.db import IntegrityError
from django.db import transaction
from django.db.models import Q

from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.forms.models import model_to_dict

import datetime
import calendar
import json
import locale

from datetime import date
from dateutil import relativedelta

from app.models.cobranzas import Cobranza
from app.models.recibos import Recibo
from app.models.socios import Socio

def list( request ):
	cobranzas = Cobranza.objects.all()

	data = {
		'data': []
	}

	for i in cobranzas:
		row_class = ''
		estado_badge = ''

		if i.estado == 'cancel':
			estado_badge = '<span class="badge badge-danger">Cancelada</span><br>'
			row_class = 'table-danger'

		data['data'] += [{
			'DT_RowId': i.cobranza_id,
			'actions': '<a href="#" class="btn btn-primary btn-sm" data-action="cobranzas.menu" data-cobranza-id="' + str( i.cobranza_id ) + '"><i class="fas fa-bars"></i></a>',
			'cobranza': i.cobranza_id,
			'nombre': i.nombre,
			'cuota': '$ ' + i.cuota + ',00',
			'periodo': {
				'display': str( i.periodo_mes ).zfill(2) + '/' + str( i.periodo_anio ),
				'sort': str( i.periodo_anio ) + str( i.periodo_mes ),
			},
			'notas': estado_badge + i.notas,
			'DT_RowClass': row_class,
		}]

	return JsonResponse( data )


def detalles( request ):
	cobranza_id = request.GET.get('cobranza_id')

	if cobranza_id is None:
		return JsonResponse({ 'a': 'lol' })

	selected_cobranza = Cobranza.objects.filter(cobranza_id=cobranza_id)[0]

	recibos = Recibo.objects.all()
	recibos = recibos.filter(cobranza_id=cobranza_id)

	data = model_to_dict(selected_cobranza)

	data['periodo_mes'] = str( selected_cobranza.periodo_mes ).zfill(2)
	data['periodo'] = str( selected_cobranza.periodo_mes ).zfill(2) + '/' + str( selected_cobranza.periodo_anio )
	data['total_recibos'] = recibos.count()
	data['cuota'] = '$ ' + selected_cobranza.cuota + ',00'

	if selected_cobranza.estado == 'new':
		data['estado'] = 'Nueva'

	if selected_cobranza.estado == 'cancel':
		data['estado'] = 'Cancelada'

	return JsonResponse( data )


def create_form( request ):
	locale.setlocale( locale.LC_TIME, 'es_ES.UTF-8' )

	next_month = datetime.date.today() + relativedelta.relativedelta(months=1)
	ultima_cobranza = Cobranza.objects.latest( 'cobranza_id' )

	monto_cuota = ultima_cobranza.cuota

	data = {
		'nombre': next_month.strftime( "%B de %Y" ).capitalize(),
		'mes': next_month.strftime( "%m" ),
		'anio': next_month.strftime("%Y"),
		'cuota': monto_cuota,
		'notas': '',
	}

	return JsonResponse(data)

def edit_form( request ):
	locale.setlocale( locale.LC_TIME, 'es_ES.UTF-8' )

	cobranza_id = request.GET.get( 'cobranza_id' )

	cobranza = Cobranza.objects.filter(pk=cobranza_id).get()

	data = {
		'cobranza_id': cobranza_id,
		'nombre': cobranza.nombre,
		'notas': cobranza.notas,
	}

	return JsonResponse(data)

def generate( request ):
	input_data = json.loads( request.GET.get( 'data' ) )

	data = {}

	nombre = input_data['nombre']
	anio = int( input_data['anio'] )
	mes = int( input_data['mes'] )
	monto_cuota = int( input_data['cuota'] )
	notas = input_data['notas']
	
	rango_primer_dia = date( anio, mes, 1 )
	rango_ultimo_dia = date( anio, mes, calendar.monthrange(anio, mes)[1] )

	try:
		with transaction.atomic():
			# Creamos una nueva cobranza
			nueva_cobranza = Cobranza(
				nombre=nombre,
				periodo_anio=anio,
				periodo_mes=mes,
				cuota=monto_cuota,
				notas=notas,
			)

			nueva_cobranza.save()

			id_cobranza = nueva_cobranza.cobranza_id


			# Creamos los recibos asociados
			socios_activos = Socio.objects.all()
			socios_activos = socios_activos.filter( status='active' )
			socios_activos = socios_activos.filter( alta_fecha__lte=rango_ultimo_dia )
			socios_activos = socios_activos.filter( Q( baja_fecha__gt=rango_primer_dia ) | Q( baja_fecha=None ) )
			socios_activos = socios_activos.only( 
				'socio_id',
				'categoria_id',
				'cobranza_cobrador_id',
				'apellido',
				'nombre',
				'cobranza_tipo',
				'cobranza_epago_sistema',
				'cobranza_epago_entidad_nacional',
				'cobranza_epago_entidad_local_id'
			)

			inserts = []

			for i in socios_activos:
				#print str(i.socio_id) + ' ' + str(i.cobranza_cobrador_id)

				if i.cobranza_epago_entidad_local_id == '':
					entidad_local = None
				else:
					entidad_local = i.cobranza_epago_entidad_local_id

				if 4 == i.categoria_id:
					total_recibo = monto_cuota + ( monto_cuota * i.categoria_protector_cuotas_adicionales )
				else:
					total_recibo = monto_cuota

				inserts += [
					Recibo(
						cobranza=nueva_cobranza,
						socio=i,
						categoria_id=str( i.categoria_id ),
						cobranza_tipo=str( i.cobranza_tipo ),
						cobrador_id=i.cobranza_cobrador_id,
						epago_sistema=i.cobranza_epago_sistema,
						epago_entidad_nacional=i.cobranza_epago_entidad_nacional,
						epago_entidad_local_id=entidad_local,
						recibo_afip=i.cobranza_recibo_afip,
						cuota=str( total_recibo ),
					)
				]

			recibos = Recibo.objects.bulk_create( inserts )

	except IntegrityError:
		data = {
			'exito': False,
			'message': "Se produjo un error al intentar generar la nueva cobranza.",
			'data': {},
		}

	else:
		data = {
			'exito': True,
			'message': "Se ha creado una nueva cobranza exitosamente.",
			'data': {
				'cobranza_id': nueva_cobranza.cobranza_id,
				'nombre': nueva_cobranza.nombre,
				'periodo_anio': nueva_cobranza.periodo_anio,
				'periodo_mes': nueva_cobranza.periodo_mes,
				'cuota': nueva_cobranza.cuota,
				'notas': nueva_cobranza.notas,
				'estado': nueva_cobranza.estado,
			},
		}

	return JsonResponse( data )

def cancelar(request):
	input_data = json.loads( request.GET.get( 'data' ) )

	try:
		with transaction.atomic():
			# Obtenemos la cobranza
			c = Cobranza.objects.filter(pk=input_data['cobranza_id'])
			c.update(estado="cancel")


			# Obtenemos los recibos asociados
			recibos = Recibo.objects.filter(cobranza_id=input_data['cobranza_id'])
			recibos.update(anular_anulado=True, anular_motivo="La cobranza ha sido cancelada.")

	except IntegrityError:
		data = {
			'exito': False,
			'message': "Se produjo un error al intentar cancelar la cobranza.",
			'data': {},
		}


	else:
		data = {
			'exito': True,
			'message': "Se ha cancelado la cobranza exitosamente.",
			'data': {
				'cobranza_id': input_data['cobranza_id'],
			},
		}

	return JsonResponse( data )


def update(request):
	input_data = json.loads( request.GET.get( 'data' ) )

	data = {}

	cobranza_id = input_data['id']
	nombre = input_data['nombre']
	notas = input_data['notas']

	# Obtenemos la cobranza
	try:
		c = Cobranza.objects.filter(pk=cobranza_id)
		c.update(
			nombre=nombre,
			notas=notas,
		)

		c = Cobranza.objects.get(pk=cobranza_id)

	except:
		data = {
			'exito': False,
			'message': "Se produjo un error al intentar actualizar la cobranza.",
			'data': {},
		}

	else:
		data = {
			'exito': True,
			'message': "Se ha actualizado la cobranza exitosamente.",
			'data': {
				'cobranza_id': cobranza_id,
			},
		}

	return JsonResponse( data )



