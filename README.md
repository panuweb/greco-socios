
## Contenedores

* sistema
* mariadb
* phpmyadmin


## Docker Composer Commands

Iniciar todo el servidor
    docker-compose -p greco-socios up -d --remove-orphans --build

Detener todo el servidor
    docker-compose -p greco-socios down

Mostrar logs de todo el servidor
    docker-compose -p greco-socios logs --follow



Iniciar servicio (container)
    docker-compose -p greco-socios start _nombre_del_contenedor_

Detener servicio (container)
    docker-compose -p greco-socios stop _nombre_del_contenedor_

Reiniciar servicio (container)
    docker-compose -p greco-socios stop _nombre_del_contenedor_

Mostrar logs del servicio (container)
    docker-compose -p greco-socios logs --follow _nombre_del_contenedor_

Ingresar a la consola del servicio (container)
    docker exec -it greco-socios-_nombre_del_contenedor_ bash
